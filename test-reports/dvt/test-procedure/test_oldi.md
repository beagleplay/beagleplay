Testing OLDI and touchscreen interfaces
=======================================

Prereqs
-------
* Lincoln Technology Solutions LCD185-101CT 10.1" TFT display

![LT panel 1](img/oldi_lt1.jpg?raw=true "LT 10 inch panel")
![LT panel 2](img/oldi_lt2.jpg?raw=true "LT 10 inch panel")
![LT panel 3](img/oldi_lt3.jpg?raw=true "LT 10 inch panel")
![LT panel 4](img/oldi_lt4.jpg?raw=true "LT 10 inch panel")

Assembly instructions:

* Line up the adapter board such that touch screen cable fits just right
* Tape up the adapter board to panel
* Connect touch screen cable, panel cable and two backlight cables and the the cable towards beagle
* Connect usb type C power as well
* Power up sequencing requirement: Power up panel before powering up board.

Enabling the panel:
------------------
Edit the file /boot/firmware/extlinux/extlinux.conf
and add the following line just after fdtdir

```
 fdtoverlays /overlays/k3-am625-lc-lt-lcd185.dtbo
```

Example for emmc boot:

```
label Linux eMMC
    kernel /Image
    append quiet quiet quiet console=ttyS2,115200n8 earlycon=ns16550a,mmio32,0x02800000 root=/dev/mmcblk0p2 ro rootfstype=ext4 rootwait net.ifnames=0 quiet
    fdtdir /
    fdtoverlays /overlays/k3-am625-lc-lt-lcd185.dtbo
    initrd /initrd.img
```

Testing backlight
-----------------

```
cd /sys/devices/platform/backlight/backlight/backlight
```
switch on backlight in various intensities

```
for i in $(seq 0 7); do echo -n "$i"|sudo tee brightness; sleep 2; done
```

Testing touchscreen
-------------------

Note: R243 needs to be removed before Touch Functionality is Enabled on DVT and EVT. 
TBD: Resistor Changes on Display Controller side may also be needed
![LT panel 5](img/R243_location.png?raw=true "Location on Back of Board")
![LT panel 6](img/R243_sch.png?raw=true "Schematic")

```
# evtest
No device specified, trying to scan all of /dev/input/event*
Available devices:
/dev/input/event0:	tps65219-pwrbutton
/dev/input/event1:	gpio-keys
/dev/input/event2:	Goodix Capacitive TouchScreen
/dev/input/event3:	Logitech USB Keyboard
/dev/input/event4:	Logitech USB Keyboard Consumer Control
/dev/input/event5:	Logitech USB Keyboard System Control
/dev/input/event6:	Logitech USB Optical Mouse
Select the device event number [0-6]: 2
Input driver version is 1.0.1
Input device ID: bus 0x18 vendor 0x416 product 0x3a0 version 0x1040
Input device name: "Goodix Capacitive TouchScreen"
Supported events:
  Event type 0 (EV_SYN)
  Event type 1 (EV_KEY)
    Event code 59 (KEY_F1)
    Event code 60 (KEY_F2)
    Event code 61 (KEY_F3)
    Event code 62 (KEY_F4)
    Event code 63 (KEY_F5)
    Event code 64 (KEY_F6)
    Event code 125 (KEY_LEFTMETA)
    Event code 330 (BTN_TOUCH)
  Event type 3 (EV_ABS)
    Event code 0 (ABS_X)
      Value      0
      Min        0
      Max     1919
    Event code 1 (ABS_Y)
      Value      0
      Min        0
      Max     1199
    Event code 47 (ABS_MT_SLOT)
      Value      0
      Min        0
      Max        9
    Event code 48 (ABS_MT_TOUCH_MAJOR)
      Value      0
      Min        0
      Max      255
    Event code 50 (ABS_MT_WIDTH_MAJOR)
      Value      0
      Min        0
      Max      255
    Event code 53 (ABS_MT_POSITION_X)
      Value      0
      Min        0
      Max     1919
    Event code 54 (ABS_MT_POSITION_Y)
      Value      0
      Min        0
      Max     1199
    Event code 57 (ABS_MT_TRACKING_ID)
      Value      0
      Min        0
      Max    65535
Properties:
  Property type 1 (INPUT_PROP_DIRECT)
Testing ... (interrupt to exit)
```
![LT panel 7](img/touch_working.jpg?raw=true "Touch Working")

Testing Display
---------------

Display panel should come up - See instructions similar to
[Testing HDMI](test_hdmi.md) for additional command instructions.
