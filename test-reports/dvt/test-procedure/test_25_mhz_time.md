Testing system time using 25MHz clock
=====================================

If GTC is correctly configured, then system time (as measured against realworld time measured by stop watch)
should match board time. It is a good idea to disable networking to prevent NTP from adjusting time behind our back.

you need a stop watch, and linux shell prompt on board:

```
date;sleep 120;date
```

Start stop watch at the time of pressing enter - it should print time = 120 seconds (+1 second to account for milliseconds).

```
$ date;sleep 120;date
Wed Oct 12 06:43:15 UTC 2022
Wed Oct 12 06:45:15 UTC 2022
```
