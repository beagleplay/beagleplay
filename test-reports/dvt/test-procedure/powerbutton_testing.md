Power button testing
====================


Testing with GUI:

```
sudo apt-get install -y xfce4-power-manager
sudo reboot
```

After the system has rebooted back to GUI

Press power button shortly:

![Power button](img/power_button_menu.jpg?raw=true "Power menu")


Long press:

Pressing power button for around 10-15 seconds will trigger h/w poweroff

System will power off completely

then pressing power button switches the system back online.
