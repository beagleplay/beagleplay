Test Procedure for CC1352P7
===========================

Prereqs
-------

* Two DVT boards
* [2.4Ghz antenna](https://www.amazon.com/Bingfu-Antenna-Extension-Wireless-Network/dp/B088GRM7SG)
* [SubGHz antenna](https://www.amazon.com/915MHz-Gateway-Antenna-ESP32-Extension/dp/B095JTW6XM/)

> :warning: If the antenna is mounted, you need a U.FL extraction tool such as [this](https://www.amazon.com/Hand-Tools-EXTRACTION-TOOL-U-FL-LP-04/dp/B00HKJUW98) or [this](https://www.sparkfun.com/products/19929)

![CC1352 antenna](img/cc1552p7-antenna.jpg?raw=true "CC1352 Antenna")

Packages needed:
----------------

* Firmware package
* python3 pyserial

```
sudo python3 -m pip install pyserial
```

Firmware
--------

We use Zephyr build for the testing, however, the builds have static IP address
assigned at build time and builds are for eitehr 2.4GHz OR subGHz.

So, only two boards can be tested at a time - we use IPv6 addresses,

IPs are:

Board1: 2001:db8::1
Board2: 2001:db8::2

Binaries:
---------

NM pre-built Location: /opt/beagleconnect/

Board 1:

* cc1352p7/subg/led_radio_2001_db8_1/zephyr/zephyr.bin - subghz
* cc1352p7/24g/led_radio_2001_db8_1/zephyr/zephyr.bin - 2.4GHz

Board 2:

* cc1352p7/subg/led_radio_2001_db8_2/zephyr/zephyr.bin - subghz
* cc1352p7/24g/led_radio_2001_db8_2/zephyr/zephyr.bin - 2.4GHz


Flashing instruction:
---------------------

```
cd /opt/beagleconnect/
./cc2538-bsl.py path_to_correct_binary/zephyr.bin /dev/ttyS4
```

Test 1: SubGHz
--------------

Preperation: Flash the SubGHz static IP specific binary to each board

Board1:

```
cd /opt/beagleconnect/
./cc2538-bsl.py cc1352p7/24g/led_radio_2001_db8_1/zephyr/zephyr.bin /dev/ttyS4
```

Board2:

```
cd /opt/beagleconnect/

./cc2538-bsl.py cc1352p7/24g/led_radio_2001_db8_1/zephyr/zephyr.bin /dev/ttyS4
```

Test procedure:

Board1:

```
debian@BeagleBone:~/beagleconnect$ tio -b 115200 /dev/ttyS4
[tio 04:24:24] tio v1.32
[tio 04:24:24] Press ctrl-t q to quit
[tio 04:24:24] Connected

uart:~$ net iface
Interface 0x2000154c (IEEE 802.15.4) [1]
========================================
Link addr : 11:65:84:2A:00:4B:12:00
MTU       : 125
Flags     : AUTO_START,IPv6
IPv6 unicast addresses (max 3):
	fe80::1365:842a:4b:1200 autoconf preferred infinite
	2001:db8::1 manual preferred infinite
IPv6 multicast addresses (max 4):
	ff02::1
	ff02::1:ff4b:1200
	ff02::1:ff00:1
IPv6 prefixes (max 2):
	<none>
IPv6 hop limit           : 64
IPv6 base reachable time : 30000
IPv6 reachable time      : 32854
IPv6 retransmit timer    : 0
uart:~$ net ping 2001:db8::2
PING 2001:db8::2
8 bytes from 2001:db8::2 to 2001:db8::1: icmp_seq=1 ttl=64 rssi=88 time=19 ms
8 bytes from 2001:db8::2 to 2001:db8::1: icmp_seq=2 ttl=64 rssi=88 time=8 ms
```

Board2:

```
debian@BeagleBone:~/beagleconnect$ tio -b 115200 /dev/ttyS4
[tio 04:18:17] tio v1.32
[tio 04:18:17] Press ctrl-t q to quit
[tio 04:18:17] Connected

uart:~$
uart:~$
uart:~$ net iface

Interface 0x2000154c (IEEE 802.15.4) [1]
========================================
Link addr : 4A:FC:C0:1C:00:4B:12:00
MTU       : 125
Flags     : AUTO_START,IPv6
IPv6 unicast addresses (max 3):
	fe80::48fc:c01c:4b:1200 autoconf preferred infinite
	2001:db8::2 manual preferred infinite
IPv6 multicast addresses (max 4):
	ff02::1
	ff02::1:ff4b:1200
	ff02::1:ff00:2
IPv6 prefixes (max 2):
	<none>
IPv6 hop limit           : 64
IPv6 base reachable time : 30000
IPv6 reachable time      : 16822
IPv6 retransmit timer    : 0
uart:~$ net ping 2001:db8::1
PING 2001:db8::1
8 bytes from 2001:db8::1 to 2001:db8::2: icmp_seq=0 ttl=64 rssi=93 time=8 ms
8 bytes from 2001:db8::1 to 2001:db8::2: icmp_seq=1 ttl=64 rssi=96 time=8 ms
8 bytes from 2001:db8::1 to 2001:db8::2: icmp_seq=2 ttl=64 rssi=93 time=8 ms
```

Test 2: 2.4 GHz
---------------

Preperation: Flash the 2.4GHz static IP specific binary to each board

Board1:

```
cd /opt/beagleconnect/
./cc2538-bsl.py cc1352p7/subg/led_radio_2001_db8_1/zephyr/zephyr.bin /dev/ttyS4
```

Board2:

```
cd /opt/beagleconnect/
./cc2538-bsl.py cc1352p7/subg/led_radio_2001_db8_2/zephyr/zephyr.bin /dev/ttyS4
```

Test procedure:

Board1:

```
debian@BeagleBone:~/beagleconnect$ tio -b 115200 /dev/ttyS4
[tio 04:20:29] tio v1.32
[tio 04:20:29] Press ctrl-t q to quit
[tio 04:20:29] Connected

uart:~$
uart:~$ net iface

Interface 0x20001254 (IEEE 802.15.4) [1]
========================================
Link addr : 11:65:84:2A:00:4B:12:00
MTU       : 125
Flags     : AUTO_START,IPv6
IPv6 unicast addresses (max 3):
	fe80::1365:842a:4b:1200 autoconf preferred infinite
	2001:db8::1 manual preferred infinite
IPv6 multicast addresses (max 4):
	ff02::1
	ff02::1:ff4b:1200
	ff02::1:ff00:1
IPv6 prefixes (max 2):
	<none>
IPv6 hop limit           : 64
IPv6 base reachable time : 30000
IPv6 reachable time      : 18628
IPv6 retransmit timer    : 0
uart:~$ net ping 2001:db8::2
PING 2001:db8::2
8 bytes from 2001:db8::2 to 2001:db8::1: icmp_seq=0 ttl=64 rssi=142 time=13 ms
8 bytes from 2001:db8::2 to 2001:db8::1: icmp_seq=1 ttl=64 rssi=142 time=12 ms
8 bytes from 2001:db8::2 to 2001:db8::1: icmp_seq=2 ttl=64 rssi=142 time=10 ms
uart:~$
[tio 04:22:14] Disconnected

```

Board2:

```
debian@BeagleBone:~/beagleconnect$ tio -b 115200 /dev/ttyS4
[tio 04:14:12] tio v1.32
[tio 04:14:12] Press ctrl-t q to quit
[tio 04:14:12] Connected

uart:~$
uart:~$ net iface

Interface 0x20001254 (IEEE 802.15.4) [1]
========================================
Link addr : 4A:FC:C0:1C:00:4B:12:00
MTU       : 125
Flags     : AUTO_START,IPv6
IPv6 unicast addresses (max 3):
	fe80::48fc:c01c:4b:1200 autoconf preferred infinite
	2001:db8::2 manual preferred infinite
IPv6 multicast addresses (max 4):
	ff02::1
	ff02::1:ff4b:1200
	ff02::1:ff00:2
IPv6 prefixes (max 2):
	<none>
IPv6 hop limit           : 64
IPv6 base reachable time : 30000
IPv6 reachable time      : 41445
IPv6 retransmit timer    : 0
uart:~$ ping 2001:db8::1
ping: command not found
uart:~$ net ping 2001:db8::1
PING 2001:db8::1
8 bytes from 2001:db8::1 to 2001:db8::2: icmp_seq=0 ttl=64 rssi=139 time=46 ms
8 bytes from 2001:db8::1 to 2001:db8::2: icmp_seq=1 ttl=64 rssi=139 time=10 ms
8 bytes from 2001:db8::1 to 2001:db8::2: icmp_seq=2 ttl=64 rssi=139 time=9 ms
uart:~$
[tio 04:16:11] Disconnected
```
