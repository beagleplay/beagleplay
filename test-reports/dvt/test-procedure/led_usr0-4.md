USR LED TEST
============

Switch mode to controlled by user:

```
cd /sys/devices/platform/leds/leds
for i in *; do echo -n "none">$i/trigger; done
```

switch ON all LEDs

```
for i in *; do echo -n "1">$i/brightness; done
```

Switch off all LEDs

```
for i in *; do echo -n "0">$i/brightness; done
```
