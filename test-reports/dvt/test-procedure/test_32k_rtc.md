Testing system time using 32k clock
=====================================

If 32k crystal is correctly configured, then system time (as measured against realworld time measured by stop watch)
should match onboard RTC time. It is a good idea to disable networking to prevent NTP from adjusting time behind our back.

you need a stop watch, and linux shell prompt on board:

```
cd /sys/class/rtc/rtc1
cat time;sleep 120;cat time
```

Start stop watch at the time of pressing enter - it should print time = 120 seconds (+1 second to account for milliseconds).

```
debian@BeagleBone:/sys/class/rtc/rtc1$ cat time;sleep 120;cat time
06:40:32
06:42:32
```

Test onboard RTC as well (to ensure 32k crystal for that rtc is working)

```
debian@BeagleBone:/sys/class/rtc/rtc0$ cat time;sleep 120;cat time
06:47:02
06:49:02

```
