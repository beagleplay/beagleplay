HDMI Testing
============

Preqs:
------
* usb keyboard and mouse to interact with the GUI
* HDMI monitor
* install libdrm-tests

```
sudo apt install libdrm-tests
```

a) Test with various monitors
-----------------------------

Test with different monitors

* open a terminal in the GUI
* run the command xrandr

```
$ xrandr
Screen 0: minimum 1920 x 1080, current 1920 x 1080, maximum 1920 x 1080
default connected 1920x1080+0+0 0mm x 0mm
   1920x1080      0.00*
```

* run modetest - this will confirm the edid parameters etc.

```
$ modetest -M tidss
Encoders:
id	crtc	type	possible crtcs	possible clones
38	37	none	0x00000001	0x00000001

Connectors:
id	encoder	status		name		size (mm)	modes	encoders
39	38	connected	HDMI-A-1       	520x290		31	38
  modes:
	index name refresh (Hz) hdisp hss hse htot vdisp vss vse vtot
  #0 1920x1080 60.00 1920 2008 2052 2200 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: preferred, driver
  #1 1920x1080 59.94 1920 2008 2052 2200 1080 1084 1089 1125 148352 flags: phsync, pvsync; type: driver
  #2 1920x1080 50.00 1920 2448 2492 2640 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: driver
  #3 1680x1050 59.88 1680 1728 1760 1840 1050 1053 1059 1080 119000 flags: phsync, nvsync; type: driver
  #4 1280x1024 75.02 1280 1296 1440 1688 1024 1025 1028 1066 135000 flags: phsync, pvsync; type: driver
  #5 1280x1024 60.02 1280 1328 1440 1688 1024 1025 1028 1066 108000 flags: phsync, pvsync; type: driver
  #6 1440x900 59.90 1440 1488 1520 1600 900 903 909 926 88750 flags: phsync, nvsync; type: driver
  #7 1152x864 75.00 1152 1216 1344 1600 864 865 868 900 108000 flags: phsync, pvsync; type: driver
  #8 1280x720 60.00 1280 1390 1430 1650 720 725 730 750 74250 flags: phsync, pvsync; type: driver
  #9 1280x720 59.94 1280 1390 1430 1650 720 725 730 750 74176 flags: phsync, pvsync; type: driver
  #10 1280x720 50.00 1280 1720 1760 1980 720 725 730 750 74250 flags: phsync, pvsync; type: driver
  #11 1440x576 50.00 1440 1464 1592 1728 576 581 586 625 54000 flags: nhsync, nvsync; type: driver
  #12 1024x768 75.03 1024 1040 1136 1312 768 769 772 800 78750 flags: phsync, pvsync; type: driver
  #13 1024x768 70.07 1024 1048 1184 1328 768 771 777 806 75000 flags: nhsync, nvsync; type: driver
  #14 1024x768 60.00 1024 1048 1184 1344 768 771 777 806 65000 flags: nhsync, nvsync; type: driver
  #15 1440x480 60.00 1440 1472 1596 1716 480 489 495 525 54054 flags: nhsync, nvsync; type: driver
  #16 1440x480 59.94 1440 1472 1596 1716 480 489 495 525 54000 flags: nhsync, nvsync; type: driver
  #17 832x624 74.55 832 864 928 1152 624 625 628 667 57284 flags: nhsync, nvsync; type: driver
  #18 800x600 75.00 800 816 896 1056 600 601 604 625 49500 flags: phsync, pvsync; type: driver
  #19 800x600 72.19 800 856 976 1040 600 637 643 666 50000 flags: phsync, pvsync; type: driver
  #20 800x600 60.32 800 840 968 1056 600 601 605 628 40000 flags: phsync, pvsync; type: driver
  #21 800x600 56.25 800 824 896 1024 600 601 603 625 36000 flags: phsync, pvsync; type: driver
  #22 720x576 50.00 720 732 796 864 576 581 586 625 27000 flags: nhsync, nvsync; type: driver
  #23 720x480 60.00 720 736 798 858 480 489 495 525 27027 flags: nhsync, nvsync; type: driver
  #24 720x480 59.94 720 736 798 858 480 489 495 525 27000 flags: nhsync, nvsync; type: driver
  #25 640x480 75.00 640 656 720 840 480 481 484 500 31500 flags: nhsync, nvsync; type: driver
  #26 640x480 72.81 640 664 704 832 480 489 492 520 31500 flags: nhsync, nvsync; type: driver
  #27 640x480 66.67 640 704 768 864 480 483 486 525 30240 flags: nhsync, nvsync; type: driver
  #28 640x480 60.00 640 656 752 800 480 490 492 525 25200 flags: nhsync, nvsync; type: driver
  #29 640x480 59.94 640 656 752 800 480 490 492 525 25175 flags: nhsync, nvsync; type: driver
  #30 720x400 70.08 720 738 846 900 400 412 414 449 28320 flags: nhsync, pvsync; type: driver
  props:
	1 EDID:
		flags: immutable blob
		blobs:

		value:
			00ffffffffffff000469f22333790200
			3314010380341d782ac720a455499927
			135054bfef00714f81809500b300d1c0
			010101010101023a801871382d40582c
			450009252100001e000000ff0041434c
			4d54463136323039390a000000fd0037
			4b1e5510000a202020202020000000fc
			0041535553205648323336480a20013f
			020322714f0102031112130414050e0f
			1d1e1f10230907018301000065030c00
			10008c0ad08a20e02d10103e96000925
			21000018011d007251d01e206e285500
			09252100001e011d00bc52d01e20b828
			554009252100001e8c0ad09020403120
			0c405500092521000018000000000000
			00000000000000000000000000000073
	2 DPMS:
		flags: enum
		enums: On=0 Standby=1 Suspend=2 Off=3
		value: 0
	5 link-status:
		flags: enum
		enums: Good=0 Bad=1
		value: 0
	6 non-desktop:
		flags: immutable range
		values: 0 1
		value: 0
	4 TILE:
		flags: immutable blob
		blobs:

		value:

CRTCs:
id	fb	pos	size
37	47	(0,0)	(1920x1080)
  #0 1920x1080 60.00 1920 2008 2052 2200 1080 1084 1089 1125 148500 flags: phsync, pvsync; type: preferred, driver
  props:
	24 VRR_ENABLED:
		flags: range
		values: 0 1
		value: 0
	27 CTM:
		flags: blob
		blobs:

		value:
	28 GAMMA_LUT:
		flags: blob
		blobs:

		value:
	29 GAMMA_LUT_SIZE:
		flags: immutable range
		values: 0 4294967295
		value: 256

Planes:
id	crtc	fb	CRTC x,y	x,y	gamma size	possible crtcs
31	37	47	0,0		0,0	0       	0x00000001
  formats: AR12 AB12 RA12 RG16 BG16 AR15 AB15 AR24 AB24 RA24 BA24 RG24 BG24 AR30 AB30 XR12 XB12 RX12 AR15 AB15 XR24 XB24 RX24 BX24 XR30 XB30 YUYV UYVY NV12
  props:
	8 type:
		flags: immutable enum
		enums: Overlay=0 Primary=1 Cursor=2
		value: 1
	32 zpos:
		flags: range
		values: 0 1
		value: 0
	33 COLOR_ENCODING:
		flags: enum
		enums: ITU-R BT.601 YCbCr=0 ITU-R BT.709 YCbCr=1
		value: 0
	34 COLOR_RANGE:
		flags: enum
		enums: YCbCr limited range=0 YCbCr full range=1
		value: 0
	35 alpha:
		flags: range
		values: 0 65535
		value: 65535
	36 pixel blend mode:
		flags: enum
		enums: Pre-multiplied=0 Coverage=1
		value: 0
40	0	0	0,0		0,0	0       	0x00000001
  formats: AR12 AB12 RA12 RG16 BG16 AR15 AB15 AR24 AB24 RA24 BA24 RG24 BG24 AR30 AB30 XR12 XB12 RX12 AR15 AB15 XR24 XB24 RX24 BX24 XR30 XB30 YUYV UYVY NV12
  props:
	8 type:
		flags: immutable enum
		enums: Overlay=0 Primary=1 Cursor=2
		value: 0
	41 zpos:
		flags: range
		values: 0 1
		value: 0
	42 COLOR_ENCODING:
		flags: enum
		enums: ITU-R BT.601 YCbCr=0 ITU-R BT.709 YCbCr=1
		value: 0
	43 COLOR_RANGE:
		flags: enum
		enums: YCbCr limited range=0 YCbCr full range=1
		value: 0
	44 alpha:
		flags: range
		values: 0 65535
		value: 65535
	45 pixel blend mode:
		flags: enum
		enums: Pre-multiplied=0 Coverage=1
		value: 0

Frame buffers:
id	size	pitch
```

b) Test Hot plug detect
-----------------------

The HDMI framer is at 0x4c on this board. So we can use:

```
$ cat /proc/interrupts |grep 4c
```
to test the number of interrupts we get.

plug out and check the count of interrupts
and plug in and check the count of interrupts

```
debian@BeagleBone:~$ cat /proc/interrupts |grep 4c
368:          1          0          0          0      GPIO  36 Edge    -davinci_gpio  2-004c
debian@BeagleBone:~$ echo "plug off"
plug off
debian@BeagleBone:~$ cat /proc/interrupts |grep 4c
368:          2          0          0          0      GPIO  36 Edge    -davinci_gpio  2-004c
debian@BeagleBone:~$ echo "plug back in"
plug back in
debian@BeagleBone:~$ cat /proc/interrupts |grep 4c
368:          3          0          0          0      GPIO  36 Edge    -davinci_gpio  2-004c
```

* There should'nt be infinite interrupts in the system
* The GUI should also come back online after plugging the cable back in.


b) Test HDMI Audio
------------------

Pick your fav mp3/wav/whatever file,
```
sudo apt-get install qmpp
````

* Play music, confirm audio time matches the length of music to be played.
* increase decrease volume on GUI

![Test HDMI Audio](img/hdmi_audio.jpg?raw=true "HDMI Audio")
