Testing Mikrobus
================

Preqs:
-------

a) Testing the mikroBUS Click board detection functionality requires the Click ID adapter which has the 1-wire EEPROM on which the manifest is stored.
b) you also need the manifest files -> the following steps generate the same.

On the board:

```
git clone https://github.com/vaishnavachath/manifesto.git
cd manifesto/
ls
sh install.sh
```

![Mikro01](img/mikro01.jpg?raw=true "Mikro01")
![Mikro02](img/mikro02.jpg?raw=true "Mikro02")
![Mikro03](img/mikro03.jpg?raw=true "Mikro03")
![Mikro04](img/mikro04.jpg?raw=true "Mikro04")
![Mikro05](img/mikro05.jpg?raw=true "Mikro05")
![Mikro06](img/mikro05.jpg?raw=true "Mikro06")

Test procedure:
---------------

Insert the mikrobus board before powering on the device.

mikroBUS driver has a sysfs debug interface which takes the manifest binary blob as input and performs the corresponding probe, the usage is:

```
root@BeagleBone:/home/debian/manifesto# ls /sys/class/mikrobus-port/mikrobus-0/
delete_device  device  name  new_device  of_node  power  subsystem  uevent
root@BeagleBone:/home/debian/manifesto# xxd manifests/MICROSD-CLICK.mnfb
00000000: 7c00 0001 0800 0100 0102 0000 1800 0200  |...............
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0d02 4d49 4352 4f53  ka........MICROS
00000030: 4420 436c 6963 6b00 1000 0500 0401 0707  D Click.........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 010b 1400 0700 0103 0b00  ................
00000060: 80f0 fa02 0000 0000 0000 0000 1000 0200  ................
00000070: 0703 6d6d 635f 7370 6900 0000            ..mmc_spi...
root@BeagleBone:/home/debian/manifesto# cat manifests/MICROSD-CLICK.mnfb > /sys/class/mikrobus-port/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[   53.781634] ti-bcdma 485c0100.dma-controller: chan0 teardown timeout!
[  181.016633] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=mmc_spi, protocol=11, reg=0
[  181.016665] mikrobus_manifest:mikrobus_manifest_parse:  MICROSD Click manifest parsed with 1 devices
[  181.016847] mikrobus mikrobus-0: registering device : mmc_spi
[  181.169314] mmc_spi spi1.0: ASSUMING 3.2-3.4 V slot power
[  181.193992] mmc_spi spi1.0: SD/MMC host mmc3, no WP, no poweroff
[  181.354772] mmc3: host does not support reading read-only switch, assuming write-enable
[  181.354884] mmc3: new SDHC card on SPI
[  181.358330] mmcblk3: mmc3:0000 SC16G 14.8 GiB
[  181.371462]  mmcblk3: p1
```

To remove the device:

```
 echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
```

Alpha testing env:
-----------------

* Testing has been done for OLEDC-Click, microSD Click, Weather Click, RTC-6 Click, Environment Click, GNSS-4 Click, MPU-9DOF Click
* Testing Logs(first 3 detected with Click ID adapter, rest manually probed using debug interface):

OLEDC Click :
```
	[ 22.645779] Initializing XFRM netlink socket
[ 24.769196] fb_ssd1351 spi1.0: (blank=false)
[ 26.067806] davinci-mcasp 2b10000.mcasp: Sample-rate is off by -11776 PPM
[ 26.150467] davinci-mcasp 2b10000.mcasp: Sample-rate is off by -11776 PPM
[ 26.179360] davinci-mcasp 2b10000.mcasp: Sample-rate is off by -11776 PPM
[ 32.406043] ti-bcdma 485c0100.dma-controller: chan0 teardown timeout!
debian@BeagleBone:~$ dmesg | grep mikrobus
[ 1.996580] mikrobus linux-mikrobus: failed to get gpio array [-517]
[ 2.623561] mikrobus linux-mikrobus: failed to get gpio array [-517]
[ 3.055948] mikrobus:mikrobus_port_register: registering port mikrobus-0 
[ 3.056023] mikrobus mikrobus-0: mikrobus port 0 eeprom empty probing default eeprom
[ 3.116857] mikrobus:mikrobus_port_scan_eeprom: manifest start address is 0x0 
[ 4.222039] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=fb_ssd1351, protocol=11, reg=0
[ 4.222055] mikrobus_manifest:mikrobus_manifest_attach_device: device 1, number of properties=7
[ 4.222072] mikrobus_manifest:mikrobus_manifest_attach_device: device 1, number of gpio resource=2
[ 4.222080] mikrobus_manifest:mikrobus_manifest_parse: OLEDC Click manifest parsed with 1 devices
[ 4.222177] mikrobus mikrobus-0: registering device : fb_ssd1351
[ 4.222186] mikrobus mikrobus-0: adding lookup table : spi1.0
debian@BeagleBone:~$ dmesg | grep fb_ssd1351
[ 4.222039] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=fb_ssd1351, protocol=11, reg=0
[ 4.222177] mikrobus mikrobus-0: registering device : fb_ssd1351
[ 9.590456] fb_ssd1351: module is from the staging directory, the quality is unknown, you have been warned.
[ 9.591314] fb_ssd1351 spi1.0: fbtft_property_value: width = 128
[ 9.591329] fb_ssd1351 spi1.0: fbtft_property_value: height = 128
[ 9.591336] fb_ssd1351 spi1.0: fbtft_property_value: regwidth = 8
[ 9.591342] fb_ssd1351 spi1.0: fbtft_property_value: buswidth = 8
[ 9.591348] fb_ssd1351 spi1.0: fbtft_property_value: backlight = 2
[ 9.591354] fb_ssd1351 spi1.0: fbtft_property_value: debug = 3
[ 9.591363] fb_ssd1351 spi1.0: fbtft_property_value: fps = 40
[ 9.591429] fb_ssd1351 spi1.0: fbtft_gamma_parse_str() str=
[ 9.591436] fb_ssd1351 spi1.0: 0 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2 2
[ 9.591491] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'reset' GPIO
[ 9.591506] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'dc' GPIO
[ 9.591512] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'rd' GPIO
[ 9.591517] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'wr' GPIO
[ 9.591523] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'cs' GPIO
[ 9.591531] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'latch' GPIO
[ 9.591536] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591542] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591547] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591552] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591557] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591563] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591568] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591574] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591579] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591585] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591590] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591595] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591601] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591606] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591611] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591617] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591623] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591629] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591634] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591639] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591644] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591650] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591655] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591661] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591666] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591671] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591677] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591682] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591688] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591693] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591699] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591706] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591712] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591718] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591723] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591729] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591736] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591741] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591748] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591754] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591760] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591766] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591771] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591778] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591784] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591789] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'db' GPIO
[ 9.591794] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'led' GPIO
[ 9.591800] fb_ssd1351 spi1.0: fbtft_request_one_gpio: 'aux' GPIO
[ 9.591809] fb_ssd1351 spi1.0: fbtft_verify_gpios()
[ 9.591816] fb_ssd1351 spi1.0: fbtft_reset()
[ 9.866004] fb_ssd1351 spi1.0: Display update: 230 kB/s, fps=0
[ 9.867172] graphics fb0: fb_ssd1351 frame buffer, 128x128, 32 KiB video memory, 4 KiB buffer memory, fps=41, spi1.0 at 6 MHz
[ 9.867183] fb_ssd1351 spi1.0: update_onboard_backlight: power=0, fb_blank=0
[ 14.836771] fb_ssd1351 spi1.0: (blank=true)
[ 21.681503] fb_ssd1351 spi1.0: (blank=false)
[ 21.685759] fb_ssd1351 spi1.0: update_onboard_backlight: power=0, fb_blank=0
[ 24.769196] fb_ssd1351 spi1.0: (blank=false)
debian@BeagleBone:~$ sudo fbi -T 1 -a beagle.png 
[sudo] password for debian: 
using "Bitstream Vera Sans Mono-16", pixelsize=16.67 file=/usr/share/fonts/truetype/ttf-bitstream-vera/VeraMono.ttf
debian@BeagleBone:~$ con
containerd containerd-shim containerd-shim-runc-v1 containerd-shim-runc-v2 continue convert-dtsv0 convert-json             
debian@BeagleBone:~$ sudo fbi -T 1 -a beaglelogo.png 
using "Bitstream Vera Sans Mono-16", pixelsize=16.67 file=/usr/share/fonts/truetype/ttf-bitstream-vera/VeraMono.ttf
debian@BeagleBone:~$ 
```

Weather Click :
```
debian@BeagleBone:~$
debian@BeagleBone:~$ dmesg | grep mikrobus
[ 2.001038] mikrobus linux-mikrobus: failed to get gpio array [-517]
[ 2.629281] mikrobus linux-mikrobus: failed to get gpio array [-517]
[ 3.045809] mikrobus:mikrobus_port_register: registering port mikrobus-0
[ 3.045909] mikrobus mikrobus-0: mikrobus port 0 eeprom empty probing default eeprom
[ 3.105139] mikrobus:mikrobus_port_scan_eeprom: manifest start address is 0x0
[ 3.502333] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=bme280, protocol=3, reg=76
[ 3.502350] mikrobus_manifest:mikrobus_manifest_parse: Weather Click manifest parsed with 1 devices
[ 3.502440] mikrobus mikrobus-0: registering device : bme280
debian@BeagleBone:~$ dmesg | grep bmp
[ 10.000467] bmp280 3-0076: supply vddd not found, using dummy regulator
[ 10.000744] bmp280 3-0076: supply vdda not found, using dummy regulator
debian@BeagleBone:~$ iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
 local,kernel: 5.10.120+
 uri: local:
IIO context has 2 devices:
 iio:device0: bme280
  3 channels found:
   temp: (input)
   2 channel-specific attributes found:
    attr 0: input value: 26420
    attr 1: oversampling_ratio value: 2
   pressure: (input)
   2 channel-specific attributes found:
    attr 0: input value: 91.302566406
    attr 1: oversampling_ratio value: 16
   humidityrelative: (input)
   2 channel-specific attributes found:
    attr 0: input value: 100000
    attr 1: oversampling_ratio value: 16
  No trigger on this device
 iio:device1: adc102s051
  2 channels found:
   voltage1: (input)
   2 channel-specific attributes found:
    attr 0: raw value: 4092
    attr 1: scale value: 0.805664062
   voltage0: (input)
   2 channel-specific attributes found:
    attr 0: raw value: 2508
    attr 1: scale value: 0.805664062
  No trigger on this device
debian@BeagleBone:~$ iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
 local,kernel: 5.10.120+
 uri: local:
IIO context has 2 devices:
 iio:device0: bme280
  3 channels found:
   temp: (input)
   2 channel-specific attributes found:
    attr 0: input value: 26430
    attr 1: oversampling_ratio value: 2
   pressure: (input)
   2 channel-specific attributes found:
    attr 0: input value: 91.304304687
    attr 1: oversampling_ratio value: 16
   humidityrelative: (input)
   2 channel-specific attributes found:
    attr 0: input value: 100000
    attr 1: oversampling_ratio value: 16
  No trigger on this device
 iio:device1: adc102s051
  2 channels found:
   voltage1: (input)
   2 channel-specific attributes found:
    attr 0: raw value: 4092
    attr 1: scale value: 0.805664062
   voltage0: (input)
   2 channel-specific attributes found:
    attr 0: raw value: 2672
    attr 1: scale value: 0.805664062
  No trigger on this device
debian@BeagleBone:~$ iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
 local,kernel: 5.10.120+
 uri: local:
IIO context has 2 devices:
 iio:device0: bme280
  3 channels found:
   temp: (input)
   2 channel-specific attributes found:
    attr 0: input value: 26950
    attr 1: oversampling_ratio value: 2
   pressure: (input)
   2 channel-specific attributes found:
    attr 0: input value: 91.301421875
    attr 1: oversampling_ratio value: 16
   humidityrelative: (input)
   2 channel-specific attributes found:
    attr 0: input value: 100000
    attr 1: oversampling_ratio value: 16
  No trigger on this device
 iio:device1: adc102s051
  2 channels found:
   voltage1: (input)
   2 channel-specific attributes found:
    attr 0: raw value: 4092
    attr 1: scale value: 0.805664062
   voltage0: (input)
   2 channel-specific attributes found:
    attr 0: raw value: 2964
    attr 1: scale value: 0.805664062
  No trigger on this device
debian@BeagleBone:~$

```

RTC-6 Click :

```
[ 22.349619] Initializing XFRM netlink socket
[ 36.577333] davinci-mcasp 2b10000.mcasp: Sample-rate is off by -11776 PPM
[ 36.682708] davinci-mcasp 2b10000.mcasp: Sample-rate is off by -11776 PPM
[ 36.730697] davinci-mcasp 2b10000.mcasp: Sample-rate is off by -11776 PPM
debian@BeagleBone:~$ dmesg | grep -i mikrobus
[ 2.001272] mikrobus linux-mikrobus: failed to get gpio array [-517]
[ 2.628175] mikrobus linux-mikrobus: failed to get gpio array [-517]
[ 3.052130] mikrobus:mikrobus_port_register: registering port mikrobus-0
[ 3.052256] mikrobus mikrobus-0: mikrobus port 0 eeprom empty probing default eeprom
[ 3.113423] mikrobus:mikrobus_port_scan_eeprom: manifest start address is 0x0
[ 3.322531] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=mcp7941x, protocol=3, reg=6f
[ 3.322547] mikrobus_manifest:mikrobus_manifest_parse: RTC 6 Click manifest parsed with 1 devices
[ 3.322665] mikrobus mikrobus-0: registering device : mcp7941x
debian@BeagleBone:~$ dmesg | grep -i rtc
[ 2.801269] rtc-ti-k3 2b1f0000.rtc: registered as rtc1
[ 3.152668] tidss 30200000.dss: [drm] Cannot find any crtc or sizes
[ 3.322547] mikrobus_manifest:mikrobus_manifest_parse: RTC 6 Click manifest parsed with 1 devices
[ 11.032198] bq32k 0-0068: Oscillator Failure. Check RTC battery.
[ 11.033062] bq32k 0-0068: registered as rtc0
[ 11.075883] rtc-ds1307 3-006f: registered as rtc2
debian@BeagleBone:~$ ls /sys/class/rtc/rtc2/
date dev device hctosys max_user_freq name power since_epoch subsystem time uevent
debian@BeagleBone:~$ cat /sys/class/rtc/rtc2/name
rtc-ds1307 3-006f
```

GNSS-4 Click, MPU9DOF Click, Environment Click, microSD Click :
```
root@BeagleBone:/home/debian# ls
Desktop  Documents  Downloads  Music  Pictures	Public	Templates  Videos  beagle.png  beaglelogo.png  cc2538-bsl.py  examples	manifesto  zephyr.bin
root@BeagleBone:/home/debian# cd manifesto/
root@BeagleBone:/home/debian/manifesto# ls manifests/
10DOF-CLICK.mnfb       ADC-5-CLICK.mnfs		 COMPASS-2-CLICK.mnfs	 GNSS-4-CLICK.mnfb	    I2C-MUX-CLICK		NO2-2-CLICK.mnfb	 RF-METER-CLICK.mnfs	 THERMO-15-CLICK.mnfb
10DOF-CLICK.mnfs       ADC-8-CLICK.mnfb		 COMPASS-CLICK.mnfb	 GNSS-4-CLICK.mnfs	    I2C-MUX-CLICK.mnfb		NO2-2-CLICK.mnfs	 RMS-TO-DC-CLICK.mnfb	 THERMO-15-CLICK.mnfs
13DOF-2-CLICK.mnfb     ADC-8-CLICK.mnfs		 COMPASS-CLICK.mnfs	 GNSS-7-CLICK.mnfb	    I2C-MUX-CLICK.mnfs		NO2-CLICK.mnfb		 RMS-TO-DC-CLICK.mnfs	 THERMO-17-CLICK.mnfb
13DOF-2-CLICK.mnfs     ADC-CLICK.mnfb		 CURRENT-CLICK.mnfb	 GNSS-7-CLICK.mnfs	    ILLUMINANCE-CLICK.mnfb	NO2-CLICK.mnfs		 RTC-6-CLICK.mnfb	 THERMO-17-CLICK.mnfs
3D-HALL-3-CLICK.mnfb   ADC-CLICK.mnfs		 CURRENT-CLICK.mnfs	 GNSS-ZOE-CLICK.mnfb	    ILLUMINANCE-CLICK.mnfs	OLEDB-CLICK.mnfb	 RTC-6-CLICK.mnfs	 THERMO-3-CLICK.mnfb
3D-HALL-3-CLICK.mnfs   AIR-QUALITY-2-CLICK.mnfb  DAC-7-CLICK.mnfb	 GNSS-ZOE-CLICK.mnfs	    IR-GESTURE-CLICK.mnfb	OLEDB-CLICK.mnfs	 SHT-CLICK.mnfb		 THERMO-3-CLICK.mnfs
3D-HALL-6-CLICK.mnfb   AIR-QUALITY-2-CLICK.mnfs  DAC-7-CLICK.mnfs	 GNSS_GPS_UNTESTED	    IR-GESTURE-CLICK.mnfs	OLEDC-CLICK.mnfb	 SHT-CLICK.mnfs		 THERMO-4-CLICK.mnfb
3D-HALL-6-CLICK.mnfs   AIR-QUALITY-3-CLICK.mnfb  DAC-CLICK.mnfb		 GSR-CLICK.mnfb		    IR-THERMO-2-CLICK.mnfb	OLEDC-CLICK.mnfs	 SHT1x-CLICK.mnfb	 THERMO-4-CLICK.mnfs
6DOF-IMU-2-CLICK.mnfb  AIR-QUALITY-3-CLICK.mnfs  DAC-CLICK.mnfs		 GSR-CLICK.mnfs		    IR-THERMO-2-CLICK.mnfs	OLEDW-CLICK.mnfb	 SHT1x-CLICK.mnfs	 THERMO-7-CLICK.mnfb
6DOF-IMU-2-CLICK.mnfs  AIR-QUALITY-5-CLICK.mnfb  DIGIPOT-3-CLICK.mnfb	 GYRO-2-CLICK.mnfb	    LED-DRIVER-7-CLICK.mnfb	OLEDW-CLICK.mnfs	 SMOKE-CLICK.mnfb	 THERMO-7-CLICK.mnfs
6DOF-IMU-4-CLICK.mnfb  AIR-QUALITY-5-CLICK.mnfs  DIGIPOT-3-CLICK.mnfs	 GYRO-2-CLICK.mnfs	    LED-DRIVER-7-CLICK.mnfs	OZONE-2-CLICK.mnfb	 SMOKE-CLICK.mnfs	 THERMO-8-CLICK.mnfb
6DOF-IMU-4-CLICK.mnfs  ALCOHOL-2-CLICK.mnfb	 DIGIPOT-CLICK.mnfb	 GYRO-CLICK.mnfb	    LIGHTRANGER-2-CLICK.mnfb	OZONE-2-CLICK.mnfs	 TEMP-HUM-11-CLICK.mnfb  THERMO-8-CLICK.mnfs
6DOF-IMU-6-CLICK.mnfb  ALCOHOL-2-CLICK.mnfs	 DIGIPOT-CLICK.mnfs	 GYRO-CLICK.mnfs	    LIGHTRANGER-2-CLICK.mnfs	PRESSURE-11-CLICK.mnfb	 TEMP-HUM-11-CLICK.mnfs  THERMO-CLICK.mnfb
6DOF-IMU-6-CLICK.mnfs  ALCOHOL-3-CLICK.mnfb	 EEPROM-2-CLICK.mnfb	 HALL-CURRENT-2-CLICK.mnfb  LIGHTRANGER-3-CLICK.mnfb	PRESSURE-11-CLICK.mnfs	 TEMP-HUM-12-CLICK.mnfb  THERMO-CLICK.mnfs
6DOF-IMU-8-CLICK.mnfb  ALCOHOL-3-CLICK.mnfs	 EEPROM-2-CLICK.mnfs	 HALL-CURRENT-2-CLICK.mnfs  LIGHTRANGER-3-CLICK.mnfs	PRESSURE-3-CLICK.mnfb	 TEMP-HUM-12-CLICK.mnfs  THERMOSTAT-3-CLICK.mnfb
6DOF-IMU-8-CLICK.mnfs  ALTITUDE-3-CLICK.mnfb	 EEPROM-3-CLICK.mnfb	 HALL-CURRENT-3-CLICK.mnfb  LIGHTRANGER-CLICK.mnfb	PRESSURE-3-CLICK.mnfs	 TEMP-HUM-3-CLICK.mnfb	 THERMOSTAT-3-CLICK.mnfs
9DOF-CLICK.mnfb        ALTITUDE-3-CLICK.mnfs	 EEPROM-3-CLICK.mnfs	 HALL-CURRENT-3-CLICK.mnfs  LIGHTRANGER-CLICK.mnfs	PRESSURE-4-CLICK.mnfb	 TEMP-HUM-3-CLICK.mnfs	 THUNDER-CLICK.mnfs
9DOF-CLICK.mnfs        ALTITUDE-CLICK.mnfb	 EEPROM-CLICK.mnfb	 HALL-CURRENT-4-CLICK.mnfb  LPS22HB-CLICK.mnfb		PRESSURE-4-CLICK.mnfs	 TEMP-HUM-4-CLICK.mnfb	 UV-3-CLICK.mnfb
ACCEL-3-CLICK.mnfb     ALTITUDE-CLICK.mnfs	 EEPROM-CLICK.mnfs	 HALL-CURRENT-4-CLICK.mnfs  LPS22HB-CLICK.mnfs		PRESSURE-CLICK.mnfb	 TEMP-HUM-4-CLICK.mnfs	 UV-3-CLICK.mnfs
ACCEL-3-CLICK.mnfs     AMBIENT-2-CLICK.mnfb	 ENVIRONMENT-CLICK.mnfb  HDC1000-CLICK.mnfb	    LSM303AGR-CLICK.mnfb	PRESSURE-CLICK.mnfs	 TEMP-HUM-7-CLICK.mnfb	 VACUUM-CLICK.mnfb
ACCEL-5-CLICK.mnfb     AMBIENT-2-CLICK.mnfs	 ENVIRONMENT-CLICK.mnfs  HDC1000-CLICK.mnfs	    LSM303AGR-CLICK.mnfs	PROXIMITY-10-CLICK.mnfb  TEMP-HUM-7-CLICK.mnfs	 VACUUM-CLICK.mnfs
ACCEL-5-CLICK.mnfs     AMBIENT-4-CLICK.mnfb	 ETH-CLICK.mnfb		 HEART-RATE-3-CLICK.mnfb    LSM6DSL-CLICK.mnfb		PROXIMITY-10-CLICK.mnfs  TEMP-HUM-9-CLICK.mnfb	 VOLTMETER-CLICK.mnfb
ACCEL-6-CLICK.mnfb     AMBIENT-4-CLICK.mnfs	 ETH-CLICK.mnfs		 HEART-RATE-3-CLICK.mnfs    LSM6DSL-CLICK.mnfs		PROXIMITY-2-CLICK.mnfb	 TEMP-HUM-9-CLICK.mnfs	 VOLTMETER-CLICK.mnfs
ACCEL-6-CLICK.mnfs     AMBIENT-5-CLICK.mnfb	 ETH-WIZ-CLICK.mnfb	 HEART-RATE-4-CLICK.mnfb    MAGNETIC-LINEAR-CLICK.mnfb	PROXIMITY-2-CLICK.mnfs	 TEMP-HUM-CLICK.mnfb	 WAVEFORM-CLICK.mnfb
ACCEL-8-CLICK.mnfb     AMBIENT-5-CLICK.mnfs	 ETH-WIZ-CLICK.mnfs	 HEART-RATE-4-CLICK.mnfs    MAGNETIC-LINEAR-CLICK.mnfs	PROXIMITY-5-CLICK.mnfb	 TEMP-HUM-CLICK.mnfs	 WAVEFORM-CLICK.mnfs
ACCEL-8-CLICK.mnfs     AMMETER-CLICK.mnfb	 FLASH-2-CLICK.mnfb	 HEART-RATE-5-CLICK.mnfb    MAGNETIC-ROTARY-CLICK.mnfb	PROXIMITY-5-CLICK.mnfs	 TEMP-LOG-3-CLICK.mnfb	 WEATHER-CLICK.mnfb
ACCEL-CLICK.mnfb       AMMETER-CLICK.mnfs	 FLASH-2-CLICK.mnfs	 HEART-RATE-5-CLICK.mnfs    MAGNETIC-ROTARY-CLICK.mnfs	PROXIMITY-9-CLICK.mnfb	 TEMP-LOG-3-CLICK.mnfs	 WEATHER-CLICK.mnfs
ACCEL-CLICK.mnfs       CLICKS_UNTESTED		 FLASH-CLICK.mnfb	 HEART-RATE-7-CLICK.mnfb    MICROSD-CLICK.mnfb		PROXIMITY-9-CLICK.mnfs	 TEMP-LOG-4-CLICK.mnfb
ADC-2-CLICK.mnfb       COLOR-2-CLICK.mnfb	 FLASH-CLICK.mnfs	 HEART-RATE-7-CLICK.mnfs    MICROSD-CLICK.mnfs		PROXIMITY-CLICK.mnfb	 TEMP-LOG-4-CLICK.mnfs
ADC-2-CLICK.mnfs       COLOR-2-CLICK.mnfs	 GENERIC-SPI-CLICK.mnfb  HEART-RATE-CLICK.mnfb	    MPU-9DOF-CLICK.mnfb		PROXIMITY-CLICK.mnfs	 TEMP-LOG-6-CLICK.mnfb
ADC-3-CLICK.mnfb       COLOR-7-CLICK.mnfb	 GENERIC-SPI-CLICK.mnfs  HEART-RATE-CLICK.mnfs	    MPU-9DOF-CLICK.mnfs		PWM-CLICK.mnfb		 TEMP-LOG-6-CLICK.mnfs
ADC-3-CLICK.mnfs       COLOR-7-CLICK.mnfs	 GEOMAGNETIC-CLICK.mnfb  I2C-2-SPI-CLICK.mnfb	    MPU-IMU-CLICK.mnfb		PWM-CLICK.mnfs		 THERMO-12-CLICK.mnfb
ADC-5-CLICK.mnfb       COMPASS-2-CLICK.mnfb	 GEOMAGNETIC-CLICK.mnfs  I2C-2-SPI-CLICK.mnfs	    MPU-IMU-CLICK.mnfs		RF-METER-CLICK.mnfb	 THERMO-12-CLICK.mnfs
root@BeagleBone:/home/debian/manifesto# dmesg | grep -i mikrobus
[    2.000894] mikrobus linux-mikrobus: failed to get gpio array [-517]
[    2.628026] mikrobus linux-mikrobus: failed to get gpio array [-517]
[    3.058177] mikrobus:mikrobus_port_register: registering port mikrobus-0
[    3.058261] mikrobus mikrobus-0: mikrobus port 0 eeprom empty probing default eeprom
root@BeagleBone:/home/debian/manifesto# dmesg | grep -i w1
[    1.941882] DS1WM w1 busmaster driver - (c) 2004 Szabolcs Gyurko
root@BeagleBone:/home/debian/manifesto# ls /sys/class/mikrobus-port/mikrobus-0/
delete_device  device  name  new_device  of_node  power  subsystem  uevent
root@BeagleBone:/home/debian/manifesto# xxd manifests/MICROSD-CLICK.mnfb
00000000: 7c00 0001 0800 0100 0102 0000 1800 0200  |...............
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0d02 4d49 4352 4f53  ka........MICROS
00000030: 4420 436c 6963 6b00 1000 0500 0401 0707  D Click.........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 010b 1400 0700 0103 0b00  ................
00000060: 80f0 fa02 0000 0000 0000 0000 1000 0200  ................
00000070: 0703 6d6d 635f 7370 6900 0000            ..mmc_spi...
root@BeagleBone:/home/debian/manifesto# cat manifests/MICROSD-CLICK.mnfb > /sys/class/mikrobus-port/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[   53.781634] ti-bcdma 485c0100.dma-controller: chan0 teardown timeout!
[  181.016633] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=mmc_spi, protocol=11, reg=0
[  181.016665] mikrobus_manifest:mikrobus_manifest_parse:  MICROSD Click manifest parsed with 1 devices
[  181.016847] mikrobus mikrobus-0: registering device : mmc_spi
[  181.169314] mmc_spi spi1.0: ASSUMING 3.2-3.4 V slot power
[  181.193992] mmc_spi spi1.0: SD/MMC host mmc3, no WP, no poweroff
[  181.354772] mmc3: host does not support reading read-only switch, assuming write-enable
[  181.354884] mmc3: new SDHC card on SPI
[  181.358330] mmcblk3: mmc3:0000 SC16G 14.8 GiB
[  181.371462]  mmcblk3: p1
root@BeagleBone:/home/debian/manifesto# lsblk
NAME         MAJ:MIN  RM  SIZE RO TYPE MOUNTPOINT
mmcblk0      179:0     0 14.6G  0 disk
mmcblk0boot0 179:256   0    4M  1 disk
mmcblk0boot1 179:512   0    4M  1 disk
mmcblk1      179:768   0 14.7G  0 disk
|-mmcblk1p1  179:769   0  128M  0 part /boot/firmware
`-mmcblk1p2  179:770   0 14.6G  0 part /
mmcblk3      179:1024  0 14.8G  0 disk
`-mmcblk3p1  179:1025  0    7G  0 part
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  181.016665] mikrobus_manifest:mikrobus_manifest_parse:  MICROSD Click manifest parsed with 1 devices
[  181.016847] mikrobus mikrobus-0: registering device : mmc_spi
[  181.169314] mmc_spi spi1.0: ASSUMING 3.2-3.4 V slot power
[  181.193992] mmc_spi spi1.0: SD/MMC host mmc3, no WP, no poweroff
[  181.354772] mmc3: host does not support reading read-only switch, assuming write-enable
[  181.354884] mmc3: new SDHC card on SPI
[  181.358330] mmcblk3: mmc3:0000 SC16G 14.8 GiB
[  181.371462]  mmcblk3: p1
[  213.538031] mikrobus mikrobus-0: removing device mmc_spi
[  213.538982] mmc3: SPI card removed
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto# xxd manifests/GNSS-4-CLICK.mnfb
00000000: b400 0001 0800 0100 0102 0000 1800 0200  ................
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0c02 474e 5353 2034  ka........GNSS 4
00000030: 2043 6c69 636b 0000 1000 0500 0401 0707   Click..........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 0104 1400 0700 0103 0400  ................
00000060: 0000 0000 0000 0001 0000 0000 0c00 0200  ................
00000070: 0603 6e65 6f2d 6d38 0c00 0600 0101 0401  ..neo-m8........
00000080: 0200 0000 1000 0200 0904 7072 6f70 2d6c  ..........prop-l
00000090: 696e 6b00 0c00 0600 0102 0505 8025 0000  ink..........%..
000000a0: 1400 0200 0d05 6375 7272 656e 742d 7370  ......current-sp
000000b0: 6565 6400                                eed.
root@BeagleBone:/home/debian/manifesto# cat manifests/GNSS-4-CLICK.mnfb > /sys/bus/mikrobus/
devices/           drivers/           drivers_autoprobe  drivers_probe      uevent
root@BeagleBone:/home/debian/manifesto# cat manifests/GNSS-4-CLICK.mnfb > /sys/bus/mikrobus/devices/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  156.942313] w1_master_driver w1_bus_master1: Attaching one wire slave 00.c00000000000 crc ca
[  156.953546] w1_master_driver w1_bus_master1: Family 0 for 00.c00000000000.ca is not registered.
[  227.379603] w1_master_driver w1_bus_master1: Attaching one wire slave 00.200000000000 crc 23
[  227.390715] w1_master_driver w1_bus_master1: Family 0 for 00.200000000000.23 is not registered.
[  238.923831] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=neo-m8, protocol=4, reg=0
[  238.923850] mikrobus_manifest:mikrobus_manifest_attach_device: device 1, number of properties=1
[  238.923861] mikrobus_manifest:mikrobus_manifest_parse:  GNSS 4 Click manifest parsed with 1 devices
[  238.923966] mikrobus mikrobus-0: registering device : neo-m8
[  238.942410] gnss: GNSS driver registered with major 506
[  238.949593] gnss-ubx serial0-0: supply vcc not found, using dummy regulator
root@BeagleBone:/home/debian/manifesto# cat /dev/gnss0
)?r??R?*5
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,27,46,,,28*7Froot@BeagleBone:/home/debian# ls
Desktop  Documents  Downloads  Music  Pictures	Public	Templates  Videos  beagle.png  beaglelogo.png  cc2538-bsl.py  examples	manifesto  zephyr.bin
root@BeagleBone:/home/debian# cd manifesto/
root@BeagleBone:/home/debian/manifesto# ls manifests/
10DOF-CLICK.mnfb       ADC-5-CLICK.mnfs		 COMPASS-2-CLICK.mnfs	 GNSS-4-CLICK.mnfb	    I2C-MUX-CLICK		NO2-2-CLICK.mnfb	 RF-METER-CLICK.mnfs	 THERMO-15-CLICK.mnfb
10DOF-CLICK.mnfs       ADC-8-CLICK.mnfb		 COMPASS-CLICK.mnfb	 GNSS-4-CLICK.mnfs	    I2C-MUX-CLICK.mnfb		NO2-2-CLICK.mnfs	 RMS-TO-DC-CLICK.mnfb	 THERMO-15-CLICK.mnfs
13DOF-2-CLICK.mnfb     ADC-8-CLICK.mnfs		 COMPASS-CLICK.mnfs	 GNSS-7-CLICK.mnfb	    I2C-MUX-CLICK.mnfs		NO2-CLICK.mnfb		 RMS-TO-DC-CLICK.mnfs	 THERMO-17-CLICK.mnfb
13DOF-2-CLICK.mnfs     ADC-CLICK.mnfb		 CURRENT-CLICK.mnfb	 GNSS-7-CLICK.mnfs	    ILLUMINANCE-CLICK.mnfb	NO2-CLICK.mnfs		 RTC-6-CLICK.mnfb	 THERMO-17-CLICK.mnfs
3D-HALL-3-CLICK.mnfb   ADC-CLICK.mnfs		 CURRENT-CLICK.mnfs	 GNSS-ZOE-CLICK.mnfb	    ILLUMINANCE-CLICK.mnfs	OLEDB-CLICK.mnfb	 RTC-6-CLICK.mnfs	 THERMO-3-CLICK.mnfb
3D-HALL-3-CLICK.mnfs   AIR-QUALITY-2-CLICK.mnfb  DAC-7-CLICK.mnfb	 GNSS-ZOE-CLICK.mnfs	    IR-GESTURE-CLICK.mnfb	OLEDB-CLICK.mnfs	 SHT-CLICK.mnfb		 THERMO-3-CLICK.mnfs
3D-HALL-6-CLICK.mnfb   AIR-QUALITY-2-CLICK.mnfs  DAC-7-CLICK.mnfs	 GNSS_GPS_UNTESTED	    IR-GESTURE-CLICK.mnfs	OLEDC-CLICK.mnfb	 SHT-CLICK.mnfs		 THERMO-4-CLICK.mnfb
3D-HALL-6-CLICK.mnfs   AIR-QUALITY-3-CLICK.mnfb  DAC-CLICK.mnfb		 GSR-CLICK.mnfb		    IR-THERMO-2-CLICK.mnfb	OLEDC-CLICK.mnfs	 SHT1x-CLICK.mnfb	 THERMO-4-CLICK.mnfs
6DOF-IMU-2-CLICK.mnfb  AIR-QUALITY-3-CLICK.mnfs  DAC-CLICK.mnfs		 GSR-CLICK.mnfs		    IR-THERMO-2-CLICK.mnfs	OLEDW-CLICK.mnfb	 SHT1x-CLICK.mnfs	 THERMO-7-CLICK.mnfb
6DOF-IMU-2-CLICK.mnfs  AIR-QUALITY-5-CLICK.mnfb  DIGIPOT-3-CLICK.mnfb	 GYRO-2-CLICK.mnfb	    LED-DRIVER-7-CLICK.mnfb	OLEDW-CLICK.mnfs	 SMOKE-CLICK.mnfb	 THERMO-7-CLICK.mnfs
6DOF-IMU-4-CLICK.mnfb  AIR-QUALITY-5-CLICK.mnfs  DIGIPOT-3-CLICK.mnfs	 GYRO-2-CLICK.mnfs	    LED-DRIVER-7-CLICK.mnfs	OZONE-2-CLICK.mnfb	 SMOKE-CLICK.mnfs	 THERMO-8-CLICK.mnfb
6DOF-IMU-4-CLICK.mnfs  ALCOHOL-2-CLICK.mnfb	 DIGIPOT-CLICK.mnfb	 GYRO-CLICK.mnfb	    LIGHTRANGER-2-CLICK.mnfb	OZONE-2-CLICK.mnfs	 TEMP-HUM-11-CLICK.mnfb  THERMO-8-CLICK.mnfs
6DOF-IMU-6-CLICK.mnfb  ALCOHOL-2-CLICK.mnfs	 DIGIPOT-CLICK.mnfs	 GYRO-CLICK.mnfs	    LIGHTRANGER-2-CLICK.mnfs	PRESSURE-11-CLICK.mnfb	 TEMP-HUM-11-CLICK.mnfs  THERMO-CLICK.mnfb
6DOF-IMU-6-CLICK.mnfs  ALCOHOL-3-CLICK.mnfb	 EEPROM-2-CLICK.mnfb	 HALL-CURRENT-2-CLICK.mnfb  LIGHTRANGER-3-CLICK.mnfb	PRESSURE-11-CLICK.mnfs	 TEMP-HUM-12-CLICK.mnfb  THERMO-CLICK.mnfs
6DOF-IMU-8-CLICK.mnfb  ALCOHOL-3-CLICK.mnfs	 EEPROM-2-CLICK.mnfs	 HALL-CURRENT-2-CLICK.mnfs  LIGHTRANGER-3-CLICK.mnfs	PRESSURE-3-CLICK.mnfb	 TEMP-HUM-12-CLICK.mnfs  THERMOSTAT-3-CLICK.mnfb
6DOF-IMU-8-CLICK.mnfs  ALTITUDE-3-CLICK.mnfb	 EEPROM-3-CLICK.mnfb	 HALL-CURRENT-3-CLICK.mnfb  LIGHTRANGER-CLICK.mnfb	PRESSURE-3-CLICK.mnfs	 TEMP-HUM-3-CLICK.mnfb	 THERMOSTAT-3-CLICK.mnfs
9DOF-CLICK.mnfb        ALTITUDE-3-CLICK.mnfs	 EEPROM-3-CLICK.mnfs	 HALL-CURRENT-3-CLICK.mnfs  LIGHTRANGER-CLICK.mnfs	PRESSURE-4-CLICK.mnfb	 TEMP-HUM-3-CLICK.mnfs	 THUNDER-CLICK.mnfs
9DOF-CLICK.mnfs        ALTITUDE-CLICK.mnfb	 EEPROM-CLICK.mnfb	 HALL-CURRENT-4-CLICK.mnfb  LPS22HB-CLICK.mnfb		PRESSURE-4-CLICK.mnfs	 TEMP-HUM-4-CLICK.mnfb	 UV-3-CLICK.mnfb
ACCEL-3-CLICK.mnfb     ALTITUDE-CLICK.mnfs	 EEPROM-CLICK.mnfs	 HALL-CURRENT-4-CLICK.mnfs  LPS22HB-CLICK.mnfs		PRESSURE-CLICK.mnfb	 TEMP-HUM-4-CLICK.mnfs	 UV-3-CLICK.mnfs
ACCEL-3-CLICK.mnfs     AMBIENT-2-CLICK.mnfb	 ENVIRONMENT-CLICK.mnfb  HDC1000-CLICK.mnfb	    LSM303AGR-CLICK.mnfb	PRESSURE-CLICK.mnfs	 TEMP-HUM-7-CLICK.mnfb	 VACUUM-CLICK.mnfb
ACCEL-5-CLICK.mnfb     AMBIENT-2-CLICK.mnfs	 ENVIRONMENT-CLICK.mnfs  HDC1000-CLICK.mnfs	    LSM303AGR-CLICK.mnfs	PROXIMITY-10-CLICK.mnfb  TEMP-HUM-7-CLICK.mnfs	 VACUUM-CLICK.mnfs
ACCEL-5-CLICK.mnfs     AMBIENT-4-CLICK.mnfb	 ETH-CLICK.mnfb		 HEART-RATE-3-CLICK.mnfb    LSM6DSL-CLICK.mnfb		PROXIMITY-10-CLICK.mnfs  TEMP-HUM-9-CLICK.mnfb	 VOLTMETER-CLICK.mnfb
ACCEL-6-CLICK.mnfb     AMBIENT-4-CLICK.mnfs	 ETH-CLICK.mnfs		 HEART-RATE-3-CLICK.mnfs    LSM6DSL-CLICK.mnfs		PROXIMITY-2-CLICK.mnfb	 TEMP-HUM-9-CLICK.mnfs	 VOLTMETER-CLICK.mnfs
ACCEL-6-CLICK.mnfs     AMBIENT-5-CLICK.mnfb	 ETH-WIZ-CLICK.mnfb	 HEART-RATE-4-CLICK.mnfb    MAGNETIC-LINEAR-CLICK.mnfb	PROXIMITY-2-CLICK.mnfs	 TEMP-HUM-CLICK.mnfb	 WAVEFORM-CLICK.mnfb
ACCEL-8-CLICK.mnfb     AMBIENT-5-CLICK.mnfs	 ETH-WIZ-CLICK.mnfs	 HEART-RATE-4-CLICK.mnfs    MAGNETIC-LINEAR-CLICK.mnfs	PROXIMITY-5-CLICK.mnfb	 TEMP-HUM-CLICK.mnfs	 WAVEFORM-CLICK.mnfs
ACCEL-8-CLICK.mnfs     AMMETER-CLICK.mnfb	 FLASH-2-CLICK.mnfb	 HEART-RATE-5-CLICK.mnfb    MAGNETIC-ROTARY-CLICK.mnfb	PROXIMITY-5-CLICK.mnfs	 TEMP-LOG-3-CLICK.mnfb	 WEATHER-CLICK.mnfb
ACCEL-CLICK.mnfb       AMMETER-CLICK.mnfs	 FLASH-2-CLICK.mnfs	 HEART-RATE-5-CLICK.mnfs    MAGNETIC-ROTARY-CLICK.mnfs	PROXIMITY-9-CLICK.mnfb	 TEMP-LOG-3-CLICK.mnfs	 WEATHER-CLICK.mnfs
ACCEL-CLICK.mnfs       CLICKS_UNTESTED		 FLASH-CLICK.mnfb	 HEART-RATE-7-CLICK.mnfb    MICROSD-CLICK.mnfb		PROXIMITY-9-CLICK.mnfs	 TEMP-LOG-4-CLICK.mnfb
ADC-2-CLICK.mnfb       COLOR-2-CLICK.mnfb	 FLASH-CLICK.mnfs	 HEART-RATE-7-CLICK.mnfs    MICROSD-CLICK.mnfs		PROXIMITY-CLICK.mnfb	 TEMP-LOG-4-CLICK.mnfs
ADC-2-CLICK.mnfs       COLOR-2-CLICK.mnfs	 GENERIC-SPI-CLICK.mnfb  HEART-RATE-CLICK.mnfb	    MPU-9DOF-CLICK.mnfb		PROXIMITY-CLICK.mnfs	 TEMP-LOG-6-CLICK.mnfb
ADC-3-CLICK.mnfb       COLOR-7-CLICK.mnfb	 GENERIC-SPI-CLICK.mnfs  HEART-RATE-CLICK.mnfs	    MPU-9DOF-CLICK.mnfs		PWM-CLICK.mnfb		 TEMP-LOG-6-CLICK.mnfs
ADC-3-CLICK.mnfs       COLOR-7-CLICK.mnfs	 GEOMAGNETIC-CLICK.mnfb  I2C-2-SPI-CLICK.mnfb	    MPU-IMU-CLICK.mnfb		PWM-CLICK.mnfs		 THERMO-12-CLICK.mnfb
ADC-5-CLICK.mnfb       COMPASS-2-CLICK.mnfb	 GEOMAGNETIC-CLICK.mnfs  I2C-2-SPI-CLICK.mnfs	    MPU-IMU-CLICK.mnfs		RF-METER-CLICK.mnfb	 THERMO-12-CLICK.mnfs
root@BeagleBone:/home/debian/manifesto# dmesg | grep -i mikrobus
[    2.000894] mikrobus linux-mikrobus: failed to get gpio array [-517]
[    2.628026] mikrobus linux-mikrobus: failed to get gpio array [-517]
[    3.058177] mikrobus:mikrobus_port_register: registering port mikrobus-0
[    3.058261] mikrobus mikrobus-0: mikrobus port 0 eeprom empty probing default eeprom
root@BeagleBone:/home/debian/manifesto# dmesg | grep -i w1
[    1.941882] DS1WM w1 busmaster driver - (c) 2004 Szabolcs Gyurko
root@BeagleBone:/home/debian/manifesto# ls /sys/class/mikrobus-port/mikrobus-0/
delete_device  device  name  new_device  of_node  power  subsystem  uevent
root@BeagleBone:/home/debian/manifesto# xxd manifests/MICROSD-CLICK.mnfb
00000000: 7c00 0001 0800 0100 0102 0000 1800 0200  |...............
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0d02 4d49 4352 4f53  ka........MICROS
00000030: 4420 436c 6963 6b00 1000 0500 0401 0707  D Click.........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 010b 1400 0700 0103 0b00  ................
00000060: 80f0 fa02 0000 0000 0000 0000 1000 0200  ................
00000070: 0703 6d6d 635f 7370 6900 0000            ..mmc_spi...
root@BeagleBone:/home/debian/manifesto# cat manifests/MICROSD-CLICK.mnfb > /sys/class/mikrobus-port/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[   53.781634] ti-bcdma 485c0100.dma-controller: chan0 teardown timeout!
[  181.016633] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=mmc_spi, protocol=11, reg=0
[  181.016665] mikrobus_manifest:mikrobus_manifest_parse:  MICROSD Click manifest parsed with 1 devices
[  181.016847] mikrobus mikrobus-0: registering device : mmc_spi
[  181.169314] mmc_spi spi1.0: ASSUMING 3.2-3.4 V slot power
[  181.193992] mmc_spi spi1.0: SD/MMC host mmc3, no WP, no poweroff
[  181.354772] mmc3: host does not support reading read-only switch, assuming write-enable
[  181.354884] mmc3: new SDHC card on SPI
[  181.358330] mmcblk3: mmc3:0000 SC16G 14.8 GiB
[  181.371462]  mmcblk3: p1
root@BeagleBone:/home/debian/manifesto# lsblk
NAME         MAJ:MIN  RM  SIZE RO TYPE MOUNTPOINT
mmcblk0      179:0     0 14.6G  0 disk
mmcblk0boot0 179:256   0    4M  1 disk
mmcblk0boot1 179:512   0    4M  1 disk
mmcblk1      179:768   0 14.7G  0 disk
|-mmcblk1p1  179:769   0  128M  0 part /boot/firmware
`-mmcblk1p2  179:770   0 14.6G  0 part /
mmcblk3      179:1024  0 14.8G  0 disk
`-mmcblk3p1  179:1025  0    7G  0 part
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  181.016665] mikrobus_manifest:mikrobus_manifest_parse:  MICROSD Click manifest parsed with 1 devices
[  181.016847] mikrobus mikrobus-0: registering device : mmc_spi
[  181.169314] mmc_spi spi1.0: ASSUMING 3.2-3.4 V slot power
[  181.193992] mmc_spi spi1.0: SD/MMC host mmc3, no WP, no poweroff
[  181.354772] mmc3: host does not support reading read-only switch, assuming write-enable
[  181.354884] mmc3: new SDHC card on SPI
[  181.358330] mmcblk3: mmc3:0000 SC16G 14.8 GiB
[  181.371462]  mmcblk3: p1
[  213.538031] mikrobus mikrobus-0: removing device mmc_spi
[  213.538982] mmc3: SPI card removed
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto# xxd manifests/GNSS-4-CLICK.mnfb
00000000: b400 0001 0800 0100 0102 0000 1800 0200  ................
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0c02 474e 5353 2034  ka........GNSS 4
00000030: 2043 6c69 636b 0000 1000 0500 0401 0707   Click..........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 0104 1400 0700 0103 0400  ................
00000060: 0000 0000 0000 0001 0000 0000 0c00 0200  ................
00000070: 0603 6e65 6f2d 6d38 0c00 0600 0101 0401  ..neo-m8........
00000080: 0200 0000 1000 0200 0904 7072 6f70 2d6c  ..........prop-l
00000090: 696e 6b00 0c00 0600 0102 0505 8025 0000  ink..........%..
000000a0: 1400 0200 0d05 6375 7272 656e 742d 7370  ......current-sp
000000b0: 6565 6400                                eed.
root@BeagleBone:/home/debian/manifesto# cat manifests/GNSS-4-CLICK.mnfb > /sys/bus/mikrobus/
devices/           drivers/           drivers_autoprobe  drivers_probe      uevent
root@BeagleBone:/home/debian/manifesto# cat manifests/GNSS-4-CLICK.mnfb > /sys/bus/mikrobus/devices/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  156.942313] w1_master_driver w1_bus_master1: Attaching one wire slave 00.c00000000000 crc ca
[  156.953546] w1_master_driver w1_bus_master1: Family 0 for 00.c00000000000.ca is not registered.
[  227.379603] w1_master_driver w1_bus_master1: Attaching one wire slave 00.200000000000 crc 23
[  227.390715] w1_master_driver w1_bus_master1: Family 0 for 00.200000000000.23 is not registered.
[  238.923831] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=neo-m8, protocol=4, reg=0
[  238.923850] mikrobus_manifest:mikrobus_manifest_attach_device: device 1, number of properties=1
[  238.923861] mikrobus_manifest:mikrobus_manifest_parse:  GNSS 4 Click manifest parsed with 1 devices
[  238.923966] mikrobus mikrobus-0: registering device : neo-m8
[  238.942410] gnss: GNSS driver registered with major 506
[  238.949593] gnss-ubx serial0-0: supply vcc not found, using dummy regulator
root@BeagleBone:/home/debian/manifesto# cat /dev/gnss0
)?r??R?*5
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,27,46,,,28*7F
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
^C
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  156.953546] w1_master_driver w1_bus_master1: Family 0 for 00.c00000000000.ca is not registered.
[  227.379603] w1_master_driver w1_bus_master1: Attaching one wire slave 00.200000000000 crc 23
[  227.390715] w1_master_driver w1_bus_master1: Family 0 for 00.200000000000.23 is not registered.
[  238.923831] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=neo-m8, protocol=4, reg=0
[  238.923850] mikrobus_manifest:mikrobus_manifest_attach_device: device 1, number of properties=1
[  238.923861] mikrobus_manifest:mikrobus_manifest_parse:  GNSS 4 Click manifest parsed with 1 devices
[  238.923966] mikrobus mikrobus-0: registering device : neo-m8
[  238.942410] gnss: GNSS driver registered with major 506
[  238.949593] gnss-ubx serial0-0: supply vcc not found, using dummy regulator
[  278.125910] mikrobus mikrobus-0: removing device neo-m8
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto# xxd manifests/MPU-9DOF-CLICK.mnfb
00000000: 8400 0001 0800 0100 0102 0000 1800 0200  ................
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0e02 4d50 5520 3944  ka........MPU 9D
00000030: 4f46 2043 6c69 636b 1000 0500 0401 0707  OF Click........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 0102 0800 0400 0200 0103  ................
00000060: 1400 0700 0103 0368 0000 0000 0101 0000  .......h........
00000070: 0000 0000 1000 0200 0703 6d70 7539 3135  ..........mpu915
00000080: 3000 0000                                0...
root@BeagleBone:/home/debian/manifesto# cat manifests/MPU-9DOF-CLICK.mnfb > /sys/class/mikrobus-port/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  286.022126] w1_master_driver w1_bus_master1: Family 0 for 00.a00000000000.af is not registered.
[  317.712232] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=mpu9150, protocol=3, reg=68
[  317.712272] mikrobus_manifest:mikrobus_manifest_parse:  MPU 9DOF Click manifest parsed with 1 devices
[  317.712366] mikrobus mikrobus-0: registering device : mpu9150
[  317.735052] inv-mpu6050-i2c 3-0068: mounting matrix not found: using identity...
[  317.735112] inv-mpu6050-i2c 3-0068: supply vdd not found, using dummy regulator
[  317.735625] inv-mpu6050-i2c 3-0068: supply vddio not found, using dummy regulator
[  317.869100] inv-mpu6050-i2c 3-0068: whoami mismatch got 0x71 (MPU9250)expected 0x68 (MPU9150)
[  321.047117] w1_master_driver w1_bus_master1: Attaching one wire slave 00.600000000000 crc 65
[  321.058847] w1_master_driver w1_bus_master1: Family 0 for 00.600000000000.65 is not registered.
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 3 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2424
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: mpu9150 (buffer capable)
		12 channels found:
			accel_x:  (input, index: 0, format: be:S16/16>>0)
			6 channel-specific attributes found:
				attr  0: calibbias value: 8684
				attr  1: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
				attr  2: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  3: raw value: -56
				attr  4: scale value: 0.000598
				attr  5: scale_available value: 0.000598 0.001196 0.002392 0.004785
			accel_y:  (input, index: 1, format: be:S16/16>>0)
			6 channel-specific attributes found:
				attr  0: calibbias value: 3831
				attr  1: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
				attr  2: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  3: raw value: -376
				attr  4: scale value: 0.000598
				attr  5: scale_available value: 0.000598 0.001196 0.002392 0.004785
			accel_z:  (input, index: 2, format: be:S16/16>>0)
			6 channel-specific attributes found:
				attr  0: calibbias value: -2045
				attr  1: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
				attr  2: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  3: raw value: 17944
				attr  4: scale value: 0.000598
				attr  5: scale_available value: 0.000598 0.001196 0.002392 0.004785
			temp:  (input, index: 3, format: be:S16/16>>0)
			3 channel-specific attributes found:
				attr  0: offset value: 12420
				attr  1: raw value: 282
				attr  2: scale value: 2.941176
			anglvel_x:  (input, index: 4, format: be:S16/16>>0)
			5 channel-specific attributes found:
				attr  0: calibbias value: 0
				attr  1: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  2: raw value: -49
				attr  3: scale value: 0.001064724
				attr  4: scale_available value: 0.000133090 0.000266181 0.000532362 0.001064724
			anglvel_y:  (input, index: 5, format: be:S16/16>>0)
			5 channel-specific attributes found:
				attr  0: calibbias value: 0
				attr  1: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  2: raw value: -8
				attr  3: scale value: 0.001064724
				attr  4: scale_available value: 0.000133090 0.000266181 0.000532362 0.001064724
			anglvel_z:  (input, index: 6, format: be:S16/16>>0)
			5 channel-specific attributes found:
				attr  0: calibbias value: 0
				attr  1: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  2: raw value: 2
				attr  3: scale value: 0.001064724
				attr  4: scale_available value: 0.000133090 0.000266181 0.000532362 0.001064724
			magn_x:  (input, index: 7, format: be:s13/16>>0)
			3 channel-specific attributes found:
				attr  0: mount_matrix value: 0, 1, 0; 1, 0, 0; 0, 0, -1
				attr  1: raw value: 12
				attr  2: scale value: 0.003574
			magn_y:  (input, index: 8, format: be:s13/16>>0)
			3 channel-specific attributes found:
				attr  0: mount_matrix value: 0, 1, 0; 1, 0, 0; 0, 0, -1
				attr  1: raw value: -14
				attr  2: scale value: 0.003574
			magn_z:  (input, index: 9, format: be:s13/16>>0)
			3 channel-specific attributes found:
				attr  0: mount_matrix value: 0, 1, 0; 1, 0, 0; 0, 0, -1
				attr  1: raw value: -8
				attr  2: scale value: 0.003574
			timestamp:  (input, index: 10, format: le:S64/64>>0)
			gyro:  (input, WARN:iio_channel_get_type()=UNKNOWN)
			1 channel-specific attributes found:
				attr  0: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
		3 device-specific attributes found:
				attr  0: current_timestamp_clock value: realtime

				attr  1: sampling_frequency value: 50
				attr  2: sampling_frequency_available value: 10 20 50 100 200 500
		1 buffer-specific attributes found:
				attr  0: data_available value: 0
		1 debug attributes found:
				debug attr  0: direct_reg_access value: 0xBF
		Current trigger: trigger0(mpu9150-dev1)
	trigger0: mpu9150-dev1
		0 channels found:
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg |
> tail
[  317.712366] mikrobus mikrobus-0: registering device : mpu9150
[  317.735052] inv-mpu6050-i2c 3-0068: mounting matrix not found: using identity...
[  317.735112] inv-mpu6050-i2c 3-0068: supply vdd not found, using dummy regulator
[  317.735625] inv-mpu6050-i2c 3-0068: supply vddio not found, using dummy regulator
[  317.869100] inv-mpu6050-i2c 3-0068: whoami mismatch got 0x71 (MPU9250)expected 0x68 (MPU9150)
[  321.047117] w1_master_driver w1_bus_master1: Attaching one wire slave 00.600000000000 crc 65
[  321.058847] w1_master_driver w1_bus_master1: Family 0 for 00.600000000000.65 is not registered.
[  353.936059] mikrobus mikrobus-0: removing device mpu9150
[  357.620588] w1_master_driver w1_bus_master1: Attaching one wire slave 00.e00000000000 crc e9
[  357.632130] w1_master_driver w1_bus_master1: Family 0 for 00.e00000000000.e9 is not registered.
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 1 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2592
				attr  1: scale value: 0.805664062
		No trigger on this device
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto# xxd manifests/ENVIRONMENT-CLICK.mnfb
00000000: 7c00 0001 0800 0100 0102 0000 1800 0200  |...............
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1800 0200 1102 456e 7669 726f  ka........Enviro
00000030: 6e6d 656e 7420 436c 6963 6b00 1000 0500  nment Click.....
00000040: 0401 0707 0606 0505 0505 0201 0800 0300  ................
00000050: 010a 0000 0800 0400 0100 0103 1400 0700  ................
00000060: 0103 0377 0000 0000 0000 0000 0000 0000  ...w............
00000070: 0c00 0200 0603 626d 6536 3830            ......bme680
root@BeagleBone:/home/debian/manifesto# cat manifests/ENVIRONMENT-CLICK.mnfb > /sys/bus/mikrobus/
devices/           drivers/           drivers_autoprobe  drivers_probe      uevent
root@BeagleBone:/home/debian/manifesto# cat manifests/ENVIRONMENT-CLICK.mnfb > /sys/bus/mikrobus/devices/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  317.735625] inv-mpu6050-i2c 3-0068: supply vddio not found, using dummy regulator
[  317.869100] inv-mpu6050-i2c 3-0068: whoami mismatch got 0x71 (MPU9250)expected 0x68 (MPU9150)
[  321.047117] w1_master_driver w1_bus_master1: Attaching one wire slave 00.600000000000 crc 65
[  321.058847] w1_master_driver w1_bus_master1: Family 0 for 00.600000000000.65 is not registered.
[  353.936059] mikrobus mikrobus-0: removing device mpu9150
[  357.620588] w1_master_driver w1_bus_master1: Attaching one wire slave 00.e00000000000 crc e9
[  357.632130] w1_master_driver w1_bus_master1: Family 0 for 00.e00000000000.e9 is not registered.
[  399.932718] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=bme680, protocol=3, reg=77
[  399.932737] mikrobus_manifest:mikrobus_manifest_parse:  Environment Click manifest parsed with 1 devices
[  399.932832] mikrobus mikrobus-0: registering device : bme680
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 2 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2612
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: bme680
		4 channels found:
			temp:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 34380
				attr  1: oversampling_ratio value: 8
			pressure:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 693.730000000
				attr  1: oversampling_ratio value: 4
			resistance:  (input)
			1 channel-specific attributes found:
				attr  0: input ERROR: Invalid argument (22)
			humidityrelative:  (input)
			2 channel-specific attributes found:
				attr  0: input ERROR: Invalid argument (22)
				attr  1: oversampling_ratio value: 2
		1 device-specific attributes found:
				attr  0: oversampling_ratio_available value: 1 2 4 8 16
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 2 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2620
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: bme680
		4 channels found:
			temp:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 27820
				attr  1: oversampling_ratio value: 8
			pressure:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 912.940000000
				attr  1: oversampling_ratio value: 4
			resistance:  (input)
			1 channel-specific attributes found:
				attr  0: input value: 10600
			humidityrelative:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 72.404000000
				attr  1: oversampling_ratio value: 2
		1 device-specific attributes found:
				attr  0: oversampling_ratio_available value: 1 2 4 8 16
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 2 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2628
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: bme680
		4 channels found:
			temp:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 28400
				attr  1: oversampling_ratio value: 8
			pressure:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 912.960000000
				attr  1: oversampling_ratio value: 4
			resistance:  (input)
			1 channel-specific attributes found:
				attr  0: input value: 10515
			humidityrelative:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 71.940000000
				attr  1: oversampling_ratio value: 2
		1 device-specific attributes found:
				attr  0: oversampling_ratio_available value: 1 2 4 8 16
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  357.620588] w1_master_driver w1_bus_master1: Attaching one wire slave 00.e00000000000 crc e9
[  357.632130] w1_master_driver w1_bus_master1: Family 0 for 00.e00000000000.e9 is not registered.
[  399.932718] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=bme680, protocol=3, reg=77
[  399.932737] mikrobus_manifest:mikrobus_manifest_parse:  Environment Click manifest parsed with 1 devices
[  399.932832] mikrobus mikrobus-0: registering device : bme680
[  410.183208] bme680_i2c 3-0077: heater failed to reach the target temperature
[  410.191494] bme680_i2c 3-0077: reading humidity skipped
[  415.897342] w1_master_driver w1_bus_master1: Attaching one wire slave 00.100000000000 crc 9d
[  415.908799] w1_master_driver w1_bus_master1: Family 0 for 00.100000000000.9d is not registered.
[  418.310067] mikrobus mikrobus-0: removing device bme680
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 1 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4084
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2624
				attr  1: scale value: 0.805664062
		No trigger on this device
root@BeagleBone:/home/debian/manifesto#
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,28,42,,,28,46,,,28*70
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
$GNRMC,,V,,,,,,,,,,N*4D
$GNVTG,,,,,,,,,N*2E
$GNGGA,,,,,,0,00,99.99,,,,,,*56
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GNGSA,A,1,,,,,,,,,,,,,99.99,99.99,99.99*2E
$GPGSV,1,1,03,40,,,29,42,,,29,46,,,29*71
$GLGSV,1,1,00*65
$GNGLL,,,,,,V,N*7A
^C
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  156.953546] w1_master_driver w1_bus_master1: Family 0 for 00.c00000000000.ca is not registered.
[  227.379603] w1_master_driver w1_bus_master1: Attaching one wire slave 00.200000000000 crc 23
[  227.390715] w1_master_driver w1_bus_master1: Family 0 for 00.200000000000.23 is not registered.
[  238.923831] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=neo-m8, protocol=4, reg=0
[  238.923850] mikrobus_manifest:mikrobus_manifest_attach_device: device 1, number of properties=1
[  238.923861] mikrobus_manifest:mikrobus_manifest_parse:  GNSS 4 Click manifest parsed with 1 devices
[  238.923966] mikrobus mikrobus-0: registering device : neo-m8
[  238.942410] gnss: GNSS driver registered with major 506
[  238.949593] gnss-ubx serial0-0: supply vcc not found, using dummy regulator
[  278.125910] mikrobus mikrobus-0: removing device neo-m8
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto# xxd manifests/MPU-9DOF-CLICK.mnfb
00000000: 8400 0001 0800 0100 0102 0000 1800 0200  ................
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1400 0200 0e02 4d50 5520 3944  ka........MPU 9D
00000030: 4f46 2043 6c69 636b 1000 0500 0401 0707  OF Click........
00000040: 0606 0505 0505 0201 0800 0300 010a 0000  ................
00000050: 0800 0400 0100 0102 0800 0400 0200 0103  ................
00000060: 1400 0700 0103 0368 0000 0000 0101 0000  .......h........
00000070: 0000 0000 1000 0200 0703 6d70 7539 3135  ..........mpu915
00000080: 3000 0000                                0...
root@BeagleBone:/home/debian/manifesto# cat manifests/MPU-9DOF-CLICK.mnfb > /sys/class/mikrobus-port/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  286.022126] w1_master_driver w1_bus_master1: Family 0 for 00.a00000000000.af is not registered.
[  317.712232] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=mpu9150, protocol=3, reg=68
[  317.712272] mikrobus_manifest:mikrobus_manifest_parse:  MPU 9DOF Click manifest parsed with 1 devices
[  317.712366] mikrobus mikrobus-0: registering device : mpu9150
[  317.735052] inv-mpu6050-i2c 3-0068: mounting matrix not found: using identity...
[  317.735112] inv-mpu6050-i2c 3-0068: supply vdd not found, using dummy regulator
[  317.735625] inv-mpu6050-i2c 3-0068: supply vddio not found, using dummy regulator
[  317.869100] inv-mpu6050-i2c 3-0068: whoami mismatch got 0x71 (MPU9250)expected 0x68 (MPU9150)
[  321.047117] w1_master_driver w1_bus_master1: Attaching one wire slave 00.600000000000 crc 65
[  321.058847] w1_master_driver w1_bus_master1: Family 0 for 00.600000000000.65 is not registered.
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 3 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2424
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: mpu9150 (buffer capable)
		12 channels found:
			accel_x:  (input, index: 0, format: be:S16/16>>0)
			6 channel-specific attributes found:
				attr  0: calibbias value: 8684
				attr  1: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
				attr  2: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  3: raw value: -56
				attr  4: scale value: 0.000598
				attr  5: scale_available value: 0.000598 0.001196 0.002392 0.004785
			accel_y:  (input, index: 1, format: be:S16/16>>0)
			6 channel-specific attributes found:
				attr  0: calibbias value: 3831
				attr  1: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
				attr  2: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  3: raw value: -376
				attr  4: scale value: 0.000598
				attr  5: scale_available value: 0.000598 0.001196 0.002392 0.004785
			accel_z:  (input, index: 2, format: be:S16/16>>0)
			6 channel-specific attributes found:
				attr  0: calibbias value: -2045
				attr  1: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
				attr  2: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  3: raw value: 17944
				attr  4: scale value: 0.000598
				attr  5: scale_available value: 0.000598 0.001196 0.002392 0.004785
			temp:  (input, index: 3, format: be:S16/16>>0)
			3 channel-specific attributes found:
				attr  0: offset value: 12420
				attr  1: raw value: 282
				attr  2: scale value: 2.941176
			anglvel_x:  (input, index: 4, format: be:S16/16>>0)
			5 channel-specific attributes found:
				attr  0: calibbias value: 0
				attr  1: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  2: raw value: -49
				attr  3: scale value: 0.001064724
				attr  4: scale_available value: 0.000133090 0.000266181 0.000532362 0.001064724
			anglvel_y:  (input, index: 5, format: be:S16/16>>0)
			5 channel-specific attributes found:
				attr  0: calibbias value: 0
				attr  1: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  2: raw value: -8
				attr  3: scale value: 0.001064724
				attr  4: scale_available value: 0.000133090 0.000266181 0.000532362 0.001064724
			anglvel_z:  (input, index: 6, format: be:S16/16>>0)
			5 channel-specific attributes found:
				attr  0: calibbias value: 0
				attr  1: mount_matrix value: 1, 0, 0; 0, 1, 0; 0, 0, 1
				attr  2: raw value: 2
				attr  3: scale value: 0.001064724
				attr  4: scale_available value: 0.000133090 0.000266181 0.000532362 0.001064724
			magn_x:  (input, index: 7, format: be:s13/16>>0)
			3 channel-specific attributes found:
				attr  0: mount_matrix value: 0, 1, 0; 1, 0, 0; 0, 0, -1
				attr  1: raw value: 12
				attr  2: scale value: 0.003574
			magn_y:  (input, index: 8, format: be:s13/16>>0)
			3 channel-specific attributes found:
				attr  0: mount_matrix value: 0, 1, 0; 1, 0, 0; 0, 0, -1
				attr  1: raw value: -14
				attr  2: scale value: 0.003574
			magn_z:  (input, index: 9, format: be:s13/16>>0)
			3 channel-specific attributes found:
				attr  0: mount_matrix value: 0, 1, 0; 1, 0, 0; 0, 0, -1
				attr  1: raw value: -8
				attr  2: scale value: 0.003574
			timestamp:  (input, index: 10, format: le:S64/64>>0)
			gyro:  (input, WARN:iio_channel_get_type()=UNKNOWN)
			1 channel-specific attributes found:
				attr  0: matrix value: 0, 0, 0; 0, 0, 0; 0, 0, 0
		3 device-specific attributes found:
				attr  0: current_timestamp_clock value: realtime

				attr  1: sampling_frequency value: 50
				attr  2: sampling_frequency_available value: 10 20 50 100 200 500
		1 buffer-specific attributes found:
				attr  0: data_available value: 0
		1 debug attributes found:
				debug attr  0: direct_reg_access value: 0xBF
		Current trigger: trigger0(mpu9150-dev1)
	trigger0: mpu9150-dev1
		0 channels found:
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg |
> tail
[  317.712366] mikrobus mikrobus-0: registering device : mpu9150
[  317.735052] inv-mpu6050-i2c 3-0068: mounting matrix not found: using identity...
[  317.735112] inv-mpu6050-i2c 3-0068: supply vdd not found, using dummy regulator
[  317.735625] inv-mpu6050-i2c 3-0068: supply vddio not found, using dummy regulator
[  317.869100] inv-mpu6050-i2c 3-0068: whoami mismatch got 0x71 (MPU9250)expected 0x68 (MPU9150)
[  321.047117] w1_master_driver w1_bus_master1: Attaching one wire slave 00.600000000000 crc 65
[  321.058847] w1_master_driver w1_bus_master1: Family 0 for 00.600000000000.65 is not registered.
[  353.936059] mikrobus mikrobus-0: removing device mpu9150
[  357.620588] w1_master_driver w1_bus_master1: Attaching one wire slave 00.e00000000000 crc e9
[  357.632130] w1_master_driver w1_bus_master1: Family 0 for 00.e00000000000.e9 is not registered.
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 1 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2592
				attr  1: scale value: 0.805664062
		No trigger on this device
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto#
root@BeagleBone:/home/debian/manifesto# xxd manifests/ENVIRONMENT-CLICK.mnfb
00000000: 7c00 0001 0800 0100 0102 0000 1800 0200  |...............
00000010: 1001 4d69 6b72 6f45 6c65 6b74 726f 6e69  ..MikroElektroni
00000020: 6b61 0000 1800 0200 1102 456e 7669 726f  ka........Enviro
00000030: 6e6d 656e 7420 436c 6963 6b00 1000 0500  nment Click.....
00000040: 0401 0707 0606 0505 0505 0201 0800 0300  ................
00000050: 010a 0000 0800 0400 0100 0103 1400 0700  ................
00000060: 0103 0377 0000 0000 0000 0000 0000 0000  ...w............
00000070: 0c00 0200 0603 626d 6536 3830            ......bme680
root@BeagleBone:/home/debian/manifesto# cat manifests/ENVIRONMENT-CLICK.mnfb > /sys/bus/mikrobus/
devices/           drivers/           drivers_autoprobe  drivers_probe      uevent
root@BeagleBone:/home/debian/manifesto# cat manifests/ENVIRONMENT-CLICK.mnfb > /sys/bus/mikrobus/devices/mikrobus-0/new_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  317.735625] inv-mpu6050-i2c 3-0068: supply vddio not found, using dummy regulator
[  317.869100] inv-mpu6050-i2c 3-0068: whoami mismatch got 0x71 (MPU9250)expected 0x68 (MPU9150)
[  321.047117] w1_master_driver w1_bus_master1: Attaching one wire slave 00.600000000000 crc 65
[  321.058847] w1_master_driver w1_bus_master1: Family 0 for 00.600000000000.65 is not registered.
[  353.936059] mikrobus mikrobus-0: removing device mpu9150
[  357.620588] w1_master_driver w1_bus_master1: Attaching one wire slave 00.e00000000000 crc e9
[  357.632130] w1_master_driver w1_bus_master1: Family 0 for 00.e00000000000.e9 is not registered.
[  399.932718] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=bme680, protocol=3, reg=77
[  399.932737] mikrobus_manifest:mikrobus_manifest_parse:  Environment Click manifest parsed with 1 devices
[  399.932832] mikrobus mikrobus-0: registering device : bme680
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 2 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2612
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: bme680
		4 channels found:
			temp:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 34380
				attr  1: oversampling_ratio value: 8
			pressure:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 693.730000000
				attr  1: oversampling_ratio value: 4
			resistance:  (input)
			1 channel-specific attributes found:
				attr  0: input ERROR: Invalid argument (22)
			humidityrelative:  (input)
			2 channel-specific attributes found:
				attr  0: input ERROR: Invalid argument (22)
				attr  1: oversampling_ratio value: 2
		1 device-specific attributes found:
				attr  0: oversampling_ratio_available value: 1 2 4 8 16
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 2 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2620
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: bme680
		4 channels found:
			temp:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 27820
				attr  1: oversampling_ratio value: 8
			pressure:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 912.940000000
				attr  1: oversampling_ratio value: 4
			resistance:  (input)
			1 channel-specific attributes found:
				attr  0: input value: 10600
			humidityrelative:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 72.404000000
				attr  1: oversampling_ratio value: 2
		1 device-specific attributes found:
				attr  0: oversampling_ratio_available value: 1 2 4 8 16
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 2 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4092
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2628
				attr  1: scale value: 0.805664062
		No trigger on this device
	iio:device1: bme680
		4 channels found:
			temp:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 28400
				attr  1: oversampling_ratio value: 8
			pressure:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 912.960000000
				attr  1: oversampling_ratio value: 4
			resistance:  (input)
			1 channel-specific attributes found:
				attr  0: input value: 10515
			humidityrelative:  (input)
			2 channel-specific attributes found:
				attr  0: input value: 71.940000000
				attr  1: oversampling_ratio value: 2
		1 device-specific attributes found:
				attr  0: oversampling_ratio_available value: 1 2 4 8 16
		No trigger on this device
root@BeagleBone:/home/debian/manifesto# echo 0 > /sys/class/mikrobus-port/mikrobus-0/delete_device
root@BeagleBone:/home/debian/manifesto# dmesg | tail
[  357.620588] w1_master_driver w1_bus_master1: Attaching one wire slave 00.e00000000000 crc e9
[  357.632130] w1_master_driver w1_bus_master1: Family 0 for 00.e00000000000.e9 is not registered.
[  399.932718] mikrobus_manifest:mikrobus_manifest_attach_device: parsed device 1, driver=bme680, protocol=3, reg=77
[  399.932737] mikrobus_manifest:mikrobus_manifest_parse:  Environment Click manifest parsed with 1 devices
[  399.932832] mikrobus mikrobus-0: registering device : bme680
[  410.183208] bme680_i2c 3-0077: heater failed to reach the target temperature
[  410.191494] bme680_i2c 3-0077: reading humidity skipped
[  415.897342] w1_master_driver w1_bus_master1: Attaching one wire slave 00.100000000000 crc 9d
[  415.908799] w1_master_driver w1_bus_master1: Family 0 for 00.100000000000.9d is not registered.
[  418.310067] mikrobus mikrobus-0: removing device bme680
root@BeagleBone:/home/debian/manifesto# iio_info
Library version: 0.24 (git tag: v0.24)
Compiled with backends: local xml ip usb
IIO context created with local backend.
Backend version: 0.24 (git tag: v0.24)
Backend description string: Linux BeagleBone 5.10.120+ #7 SMP PREEMPT Mon Sep 26 08:16:43 IST 2022 aarch64
IIO context has 2 attributes:
	local,kernel: 5.10.120+
	uri: local:
IIO context has 1 devices:
	iio:device0: adc102s051
		2 channels found:
			voltage1:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 4084
				attr  1: scale value: 0.805664062
			voltage0:  (input)
			2 channel-specific attributes found:
				attr  0: raw value: 2624
				attr  1: scale value: 0.805664062
		No trigger on this device
root@BeagleBone:/home/debian/manifesto#
```
