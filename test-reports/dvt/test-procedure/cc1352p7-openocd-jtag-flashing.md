CC1352p7 JTAG interfacing and programming steps
===============================================

Pre-reqs
--------
Need a Linux PC
Hardware:
* [TUMPA](http://www.tiaowiki.com/w/TIAO_USB_Multi_Protocol_Adapter_User's_Manual) [See more](https://www.diygadget.com/jtag-cables-and-microcontroller-programmers/tiao-usb-multi-protocol-adapter-jtag-spi-i2c-serial)
* [idc tag connect cable](https://www.tag-connect.com/product/tc2050-idc-nl-10-pin-no-legs-cable-with-ribbon-connector)
* [retaining clip](https://www.tag-connect.com/product/tc2050-clip-3pack-retaining-clip)
* [adapter for tumpa](https://www.tag-connect.com/product/tc2050-arm2010-arm-20-pin-to-tc2050-adapter)

![Connecting to CC1352P7](img/cc1352p7_jtag.jpg?raw=true "CC1352 JTAG")

Build openOCD
-------------

First prepare the PC for building openocd (if already present - great)

```
sudo apt-get install libhidapi-dev libtool libusb-1.0-0-dev
git clone https://git.code.sf.net/p/openocd/code openocd
cd openocd
sudo cp ./contrib/60-openocd.rules /etc/udev/rules.d/
sudo udevadm control --reload-rules
./bootstrap
./configure
make
```

Configuration file for openocd
------------------------------
Create a file tcl/openocd-tumpa-laughing-coyote-cc1352p7.cfg

```
 source [find interface/ftdi/tumpa.cfg]

 transport select jtag

 # GAH JTAG ID changed from P1 to P7 variant!!!
 set CHIPNAME cc13x2
 set JRC_TAPID 0x0BB7702F
 set WORKAREASIZE 0x7000

 source [find target/ti_cc26x0.cfg]


 ftdi tdo_sample_edge falling

 # Speeds for FT2232H are in multiples of 2, and 32MHz is tops
 adapter speed 16000
```

Flashing with openOCD
---------------------

https://github.com/lc-bb/beagleconnect/tree/main/build/zephyr
you can use the sample zephyr.hex from there.

execute from openocd's tcl folder the command:

```

src/openocd/tcl$ sudo ../src/openocd -f openocd-tumpa-laughing-coyote-cc1352p7.cfg  '-c init' '-c targets' -c 'reset halt' -c 'flash write_image erase my_path_to_beagleconnect/build/zephyr/zephyr.hex' -c 'reset halt' -c 'verify_image my_path_to_beagleconnect/build/zephyr/zephyr.hex' -c 'reset run' -c shutdown

Open On-Chip Debugger 0.12.0-rc1+dev-00047-g45704698a3b0 (2022-10-13-20:05)
Licensed under GNU GPL v2
For bug reports, read
	http://openocd.org/doc/doxygen/bugs.html
adapter speed: 16000 kHz

Error: no device found
Error: unable to open ftdi device with vid 0403, pid 8a98, description '*', serial '*' at bus location '*'
Info : clock speed 16000 kHz
Info : JTAG tap: cc13x2.jrc tap/device found: 0x1bb7702f (mfg: 0x017 (Texas Instruments), part: 0xbb77, ver: 0x1)
Info : JTAG tap: cc13x2.cpu enabled
Info : [cc13x2.cpu] Cortex-M4 r0p1 processor detected
Info : [cc13x2.cpu] target has 6 breakpoints, 4 watchpoints
Info : starting gdb server for cc13x2.cpu on 3333
Info : Listening on port 3333 for gdb connections
    TargetName         Type       Endian TapName            State
--  ------------------ ---------- ------ ------------------ ------------
 0* cc13x2.cpu         cortex_m   little cc13x2.cpu         running

Info : JTAG tap: cc13x2.jrc tap/device found: 0x1bb7702f (mfg: 0x017 (Texas Instruments), part: 0xbb77, ver: 0x1)
Info : JTAG tap: cc13x2.cpu enabled
Warn : [cc13x2.cpu] Only resetting the Cortex-M core, use a reset-init event handler to reset any peripherals or configure hardware srst support.
[cc13x2.cpu] halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x000044cc msp: 0x20002280
auto erase enabled
wrote 360448 bytes from file /home/nmenon/Src/TI_Prop/Beagle/beagleconnect/build/zephyr/zephyr.hex in 1.439984s (244.447 KiB/s)

Info : JTAG tap: cc13x2.jrc tap/device found: 0x1bb7702f (mfg: 0x017 (Texas Instruments), part: 0xbb77, ver: 0x1)
Info : JTAG tap: cc13x2.cpu enabled
Warn : [cc13x2.cpu] Only resetting the Cortex-M core, use a reset-init event handler to reset any peripherals or configure hardware srst support.
[cc13x2.cpu] halted due to debug-request, current mode: Thread
xPSR: 0x01000000 pc: 0x000044cc msp: 0x20002280
verified 360448 bytes in 1.073948s (327.763 KiB/s)

Info : JTAG tap: cc13x2.jrc tap/device found: 0x1bb7702f (mfg: 0x017 (Texas Instruments), part: 0xbb77, ver: 0x1)
Info : JTAG tap: cc13x2.cpu enabled
Warn : [cc13x2.cpu] Only resetting the Cortex-M core, use a reset-init event handler to reset any peripherals or configure hardware srst support.
shutdown command invoked

```

restart the board. In theory with the correct hex file things should start working

on beaglebone:
```
$ tio -b 115200 /dev/ttyS4

```
