Power Button Short press test
=============================

install evtest

```
sudo apt-get update && sudo apt-get install evtest
```

run poweron event test
(NOTE: if GUI IS NOT enabled, systemd will attempt to automatically power off the system)

```
$ evtest
No device specified, trying to scan all of /dev/input/event*
Not running as root, no devices may be available.
Available devices:
/dev/input/event0:	tps65219-pwrbutton
Select the device event number [0-0]: 0
Input driver version is 1.0.1
Input device ID: bus 0x18 vendor 0x0 product 0x0 version 0x0
Input device name: "tps65219-pwrbutton"
Supported events:
  Event type 0 (EV_SYN)
  Event type 1 (EV_KEY)
    Event code 116 (KEY_POWER)
Properties:
Testing ... (interrupt to exit)
Event: time 1665552397.991818, type 1 (EV_KEY), code 116 (KEY_POWER), value 1
Event: time 1665552397.991818, -------------- SYN_REPORT ------------
[  OK  ] Stopped target Network is Online.
[  OK  ] Unmounted /run/user/1000.
[  OK  ] Stopped User Runtime Directory /run/user/1000.
[  OK  ] Removed slice User Slice of UID 1000.
         Stopping Permit User Sessions...
[  OK  ] Stopped Permit User Sessions.
[  OK  ] Stopped target Network.
[  OK  ] Stopped target Remote File Systems.
         Stopping Raise network interfaces...
         Stopping Network Name Resolution...
```
