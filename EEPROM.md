# EEPROM definition

This is the EEPROM definition. Please pay attention to the "Seeed update".

## Fixed header

```
#magic number
i2c mw 0x50 0x00.1 aa;
i2c mw 0x50 0x01.1 55;
i2c mw 0x50 0x02.1 33;
i2c mw 0x50 0x03.1 ee;

#type
i2c mw 0x50 0x04.1 01;

#payload size
i2c mw 0x50 0x05.1 37;
i2c mw 0x50 0x06.1 00;

#BRD_INFO - fixed 10
i2c mw 0x50 0x07.1 10;

#fixed
i2c mw 0x50 0x08.1 2e;
i2c mw 0x50 0x09.1 00;

#Board_Name - BEAGLEPLAY-A0-\0\0
i2c mw 0x50 0x0a.1 42;
i2c mw 0x50 0x0b.1 45;
i2c mw 0x50 0x0c.1 41;
i2c mw 0x50 0x0d.1 47;
i2c mw 0x50 0x0e.1 4C;
i2c mw 0x50 0x0f.1 45;
i2c mw 0x50 0x10.1 50;
i2c mw 0x50 0x11.1 4C;
i2c mw 0x50 0x12.1 41;
i2c mw 0x50 0x13.1 59;
i2c mw 0x50 0x14.1 2D;
i2c mw 0x50 0x15.1 41;
i2c mw 0x50 0x16.1 30;
i2c mw 0x50 0x17.1 2D;
i2c mw 0x50 0x18.1 00;
i2c mw 0x50 0x19.1 00;
```

## Major design revision

It has been known that Linux and u-boot developers might use this in their boot process without forward compatibility. Only update this when
there is a compatibility issue that must be handled in software.

```
#Design_Rev - 02
i2c mw 0x50 0x1a.1 30;
i2c mw 0x50 0x1b.1 32;
```

## Fixed

```
#PROC_Nbr - 0078 (SK)
i2c mw 0x50 0x1c.1 30;
i2c mw 0x50 0x1d.1 30;
i2c mw 0x50 0x1e.1 37;
i2c mw 0x50 0x1f.1 38;
```

## Minor design revision

Please update this any time there is a hardware change, including PCB, mechanical, or BOM changes. This gives users the ability to search
the documentation for what changed.

Rev A1 (pilot): 01.02.00.01
Rev A2 (production): 01.03.01.01

```
#Variant - 01
i2c mw 0x50 0x20.1 30;
i2c mw 0x50 0x21.1 31;

#PCB_Rev - 03
i2c mw 0x50 0x22.1 30;
i2c mw 0x50 0x23.1 33;

#SCHBOM_Rev - 01
i2c mw 0x50 0x24.1 30;
i2c mw 0x50 0x25.1 31;

#SWR_Rev - 01
i2c mw 0x50 0x26.1 30;
i2c mw 0x50 0x27.1 31;
```

## BeagleBoard's vendor ID

```
#VendorID - 64
i2c mw 0x50 0x28.1 36;
i2c mw 0x50 0x29.1 34;
```

## Seeed to update regularly

Be sure to update this on a for every single board

```
#Build_Week - 'WW - seeed update...
i2c mw 0x50 0x2a.1 57;
i2c mw 0x50 0x2b.1 57;

#Build_Year - 23
i2c mw 0x50 0x2c.1 32;
i2c mw 0x50 0x2d.1 33;

#BoardID - BBBBBB - Seeed update
i2c mw 0x50 0x2e.1 42;
i2c mw 0x50 0x2f.1 42;
i2c mw 0x50 0x30.1 42;
i2c mw 0x50 0x31.1 42;
i2c mw 0x50 0x32.1 42;
i2c mw 0x50 0x33.1 42;

#Serial_Nbr - SSSS
i2c mw 0x50 0x34.1 53;
i2c mw 0x50 0x35.1 53;
i2c mw 0x50 0x36.1 53;
i2c mw 0x50 0x37.1 53;
```

# DDR

```
Type - 
i2c mw 0x50 0x38.1 11;

Length -
i2c mw 0x50 0x39.1 02;
i2c mw 0x50 0x3a.1 00;

DDR Contrl -
i2c mw 0x50 0x3b.1 60;
i2c mw 0x50 0x3c.1 7d;

End frame -
i2c mw 0x50 0x3d.1 fe;
```
